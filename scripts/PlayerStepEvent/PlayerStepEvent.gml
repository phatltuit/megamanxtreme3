// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerStepEvent(){
	if !isDead {
		if !isFreeze{
			if !isReady {
				PlayerTeleport();
				y+=vsp;
			}else{
				PlayerCamera();
	
				PlayerInput();

				PlayerMove();

				PlayerAttack();
				
				PlayerSkillsAttack();

				PlayerDash();

				PlayerJump();
		
				PlayerGotHit();
		
				PlayerStateMachine();

				PlayerCollision();

				PlayerSprites();
			}
		}
		PlayerSwitchSection();
		//Up Slope
		PlayerGoUpSlope();	
		x += hsp;
		//Down Slope
		PlayerGoDownSlope();
		y += vsp;
	}else{
		PlayerDead();
	}	
}