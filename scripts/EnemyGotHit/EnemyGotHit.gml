// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function EnemyGotHit(){
	var playerBullet = instance_place(x , y, objPlayerProjectile);
	if playerBullet >= 0  and !isHit and !isDead{
		hitCounterMax = 3;
		if playerBullet.damage >= hp {				
			isDead = true;
			state = EnemyState.DeadInside;
			EnemyEffect(spr_enemy_small_explosion, 1,x,y);		
		}else{						
			hp -= playerBullet.damage;
			isHit = true;	
			EnemyEffect(spr_enemy_small_hit, 1,playerBullet.x,playerBullet.y);		
			//if IsDestroyable(playerBullet.sprite_index){
			//	instance_destroy(playerBullet);
			//}
		}
	}
	
	var playerSaber = instance_place(x , y, objSaber);
	if playerSaber >= 0 and !isHit and !isDead{
		hitCounterMax = 5;
		if playerSaber.damage >= hp{	
			isDead = true;
			state = EnemyState.DeadInside;
			EnemyEffect(spr_enemy_small_explosion, 1,x,y);					
		}else{						
			hp -= playerSaber.damage;
			isHit = true;
		}
	}
	
	if isHit {
		hitCounter++;
		if playerSaber >=0 {
			if playerSaber.x > x {
				X = bbox_right;
			}else{
				X = bbox_left;	
			}
			EnemyEffect(spr_enemy_small_hit, 1, X, y);			
		}
		switch (hitCounter/2 mod 3) {
		    case 0:
		        image_alpha = 0;
		        break;
				
			case 1:
		        image_alpha = 0.5;
		        break;
				
			case 2:
		        image_alpha = 1;
		        break;
		    default:
		        // code here
		        break;
		}
		if hitCounter >= hitCounterMax {
			hitCounter = 0;
			isHit = false;
			image_alpha = 1;
		}
	}
}