// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SonicBurst(){
	if hp <= 16 {
		ultimate = true;
		if !phaseStart {
			phaseStart = true;
			face_to_player();
			isGuard = true;
			set_sprite(spr_panther_final);						
		}	
		if phaseCounter <= 300 and phaseCounter >= 60{			
			if phaseCounter mod 60 == 0 {
				image_index = 2;
				var i = 0;					
				switch (phaseCounter / 60) {					
				    case 1:
					case 3:
						var i = 0;
						repeat 5 {
							var atk = instance_create_depth(x, y, depth-100,objBossAtk);
							set_sprite_of(atk, spr_panther_sonic_boom_3_a);	
							atk.speed = 6;
							atk.alarm[0] = 180;
							atk.direction = i;
							atk.image_angle = i + 180;
							i+=45;
						}	
						break;
				        
					case 2:
					case 4:		
						var i = 48;
						repeat 2 {
							var atk = instance_create_depth(x+i, get_view_y(), depth-100,objBossAtk);
							set_sprite_of(atk, spr_panther_sonic_boom_3_c);	
							atk.vspeed = 5;
							atk.alarm[0] = atk.vspeed/get_view_h();
							i+=96;
						}	
						var i = -48;
						repeat 2 {
							var atk = instance_create_depth(x+i, get_view_y(), depth-100,objBossAtk);
							set_sprite_of(atk, spr_panther_sonic_boom_3_c);	
							atk.vspeed = 5;
							atk.alarm[0] = atk.vspeed/get_view_h();
							i-=96;
						}
				        break;
				    default:
				        // code here
				        break;
				}	
			}
		}
		
		WaitForNextPhase(320, 0);	
	}else{
		WaitForNextPhase(1, 0);	
	}
}