// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function IsDestroyable(weapon){
	switch (weapon) {
	    case spr_x_buster_lv1:
	    case spr_x_buster_lv2:
	    case spr_x_buster_lv3:		
	        return true;
	        break;
					
	    default:
	        return false;
	        break;
	}
}