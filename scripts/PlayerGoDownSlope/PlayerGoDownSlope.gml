// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerGoDownSlope(){
	//Down slope
	if !place_meeting(x,y,objSlope) and vsp >= 0 and (place_meeting(x,y + 2 + 3*abs(hsp),objSlope) or tilemap_get_at_pixel(tilemap, image_xscale==1?bbox_left:bbox_right, bbox_bottom + 2 + abs(hsp)) != 0){			
		if place_meeting(x,y+2+abs(hsp),objSlope){
			while !place_meeting(x,y + 1,objSlope) {
				y += 1;
			}
		}else if tilemap_get_at_pixel(tilemap, image_xscale==1?bbox_left:bbox_right, bbox_bottom + 2 + abs(hsp)) != 0 {
			while tilemap_get_at_pixel(tilemap, image_xscale==1?bbox_left:bbox_right, bbox_bottom + 1) == 0 {
				y += 1;
			}
		}
		on_ground = true;
	}
	
	//Vertical Collisionsd
	var slope = instance_place(x,y+vsp,objSlope);
	if slope >= 0{
		if vsp >= 0 {
			y = slope.y;
			while place_meeting(x,y,objSlope) y--;
			vsp = 0;
			//Reset variables like ground tile collision
			isWallSlide = false;
			if isClimb {
				isClimb = false;	
			}			
			on_ground = true;	
			canPlaySFX[4] = true;
			play_sfx(2, sfx_walk);	
		}else{
			y = slope.y;
			while place_meeting(x,y,objSlope) y++;
			vsp = 0;
		}
	}
}