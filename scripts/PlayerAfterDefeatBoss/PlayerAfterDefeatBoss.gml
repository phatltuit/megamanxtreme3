// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerAfterDefeatBoss(){
	if isMissionComplete {
		isHyperJump = false;
		isDash = false;
		
		if !sprite_is(sprVictoryPose) and !sprite_is(sprRecall){
			set_sprite(sprVictoryPose);	
		}
		
		if sprite_is(sprVictoryPose) and animation_end() {
				play_sfx(7, sfx_victory);
				set_sprite(sprRecall);	
		}		
		
		if animation_end() and sprite_is(sprRecall){ 
			frame_stop(image_number - 1)
			vsp = - jumpSpd;
		}
		
		y += vsp;
		if vsp != 0 and bbox_bottom < get_view_y() {
			game_restart();	
		}
		
	}
}