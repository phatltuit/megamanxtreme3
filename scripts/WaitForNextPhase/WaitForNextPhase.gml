// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function WaitForNextPhase(wait_time, next_phase){
	if phaseCounter < wait_time {
		phaseCounter++;	
	}else{
		isGuard = false;
		phaseCounter = 0;
		jumpCounter = 0;
		phaseStart = false;
		phase = next_phase;
		rushCounter = 0;
		isAttack = false;
	}
} 