// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function get_view_w(){
	return camera_get_view_width(view_camera[0]);
}