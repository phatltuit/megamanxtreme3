// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function boss_draw_hud(){
	_x_corner = get_view_x()+get_view_w()-37;
	_y_corner = get_view_y()+2;
	_x_start_lid = _x_corner + 2;
	_y_start = _y_corner + 6;
	_x_start = _x_start_lid - 3;
	_hud_icon = spr_boss_hud_header;
	draw_sprite_ext(_hud_icon, 0, _x_corner, _y_corner, 1,1, 0, c_white, 1);
	draw_sprite_ext(spr_player_hud_lid, 0, _x_start_lid, _y_start, -1,1, 0, c_white, 1);
	for (var i = 1; i <= maxHp; ++i) {	
		draw_sprite_ext(spr_player_hud_part, 0, _x_start - i*2, _y_start, 1, 1, 0, c_white, 1);	
		if isHit and i <= prev_hp and i > hp{
			draw_sprite_ext(spr_boss_hud_hp_loss, 0, _x_start - i*2, _y_start, 1, 1, 0, c_white, 1);
		}else{
			draw_sprite_ext(spr_player_hud_empty_hp, -1, _x_start - i*2, _y_start,1,1, 0, c_white, 1);
		}
	}
	
	draw_sprite_ext(spr_player_hud_lid, 0, _x_start_lid-maxHp*2-6, _y_start, 1,1,0, c_white, 1);
	
	for (var i = 1; i <= hp; ++i) {	
		if i <= hp {
			draw_sprite_ext(spr_boss_hud_hp, 0, _x_start - i*2, _y_start, 1,1, 0, c_white, 1);
		}else{
			draw_sprite_ext(spr_boss_hud_hp, 0, _x_start - i*2, _y_start, 1,1, 0, c_white, 1);	
		}
	}
}