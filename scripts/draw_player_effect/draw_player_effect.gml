// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function draw_player_effect(){
	if isDash and !isBlocked {
		draw_sprite_ext(spr_player_dash, image_index,x - image_xscale*(is_Zero()?39:26), y + 4,image_xscale, image_yscale, image_angle, image_blend,  irandom(1));
	}
	if transition_type == Transition.BossAppear and waitCounter <= 420 and waitCounter >= 120{			
		draw_sprite_ext(spr_warning_txt, image_index,get_view_center_x(), get_view_center_y(),image_xscale, image_yscale, image_angle, image_blend, (waitCounter mod 30)/10);			
	}
}