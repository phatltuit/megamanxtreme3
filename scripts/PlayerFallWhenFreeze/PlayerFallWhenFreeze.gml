// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerFallWhenFreeze(){
	if on_ground {		
		vsp = 0;
		set_sprite(sprStand);	
	}else{	
		vsp += grav;
		if vsp >= 5.75 {
			vsp = 5.75;
		}		
		set_sprite(sprJump[1]);	
	}
	PlayerJump();
	PlayerCollision();
	y+=vsp;
}