// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerCreateEvent(){
	//Tile map, using for primary collision, must be create before the player layer.
	tilemap = layer_tilemap_get_id(layer_get_id("Collision"));		
	
	//Set depth of player to the 1st priority
	depth = get_highest_depth_layer()-100;
	show_debug_message("Player depth: "+ string(depth));
	//View boudary collided with
	view_collided = ds_list_create();
	//Particle system
	particle_sys = part_system_create();
	
	ColorClass();
	
	sh_handle_range = shader_get_uniform(sh_color_replace, "range");
	global.isPaused = false; //Test pause game
	
	//State	
	isBlocked = false;
	on_ground = true;
	isDash = false;
	isClimb = false;
	minJump = true;
	isAtk = false;
	isSkillAtk = false;
	isWallSlide = false;
	isWallJump = false;
	isCharge = false;
	isReady = false;
	isDead = false;
	isFreeze = false;
	isHit = false;
	canHit = true;
	isPushBack = false;
	canCombo = true;
	isCrouch = false;
	isHyperJump = false;	
	canDouleJump = false;//Apply for Zero
	canDash = true;
	isNearWall = false;
	isRollAtk = false; //Zero only
	isGetUp = false;
	doWallJump = false;
	isMissionComplete = false;
	isOverride = false;

	//Physics
	
	life = 2;	
	maxHp = 16;
	hp = maxHp;
	prev_hp = hp;
	wp = 16;
	maxWp = 16;
	//ep = 32;
	//maxEp = 32;
	runSpd = 1.5;
	runSpeed = 1.5;
	hyperSpd = 3.5;
	climbSpd = 1.5;
	jumpSpd = 5.5;
	dashSpd = 3.5;
	wallSpd = 0.9;
	hitSpd = 0.5;
	slashSpd = 1.4;
	grav = 0.25;
	wall_side = 1;
	
	//Bbox when moving
	bbox_horizone = bbox_right;
	bbox_vertical = bbox_bottom;


	getUpCounter = 0;
	getUpCounterMax = 10;
	waitCounter = 0;
	waitCounterMax = 540;
	wallSlideCounter = 0;
	wallJumpCounter = 0;
	wallJumpCounterMax = 8;
	wallKickMax = 6;
	stepCounter = 0;
	stepCounterMax = 20;
	skillCounter = 0;
	skillCounterMax = 60;
	beginCounter = 0;
	beginCounterMax = 180;	
	deadCounter = 0;
	deadCounterMax = 300;
	dashCounter = 0;
	dashCounterMax = 30;
	hitCounter = 0;
	hitCounterMax = 90;
	pushBackCounterMax = 30;
	transitionTime = 0;
	transitionSpd = 4;
	transitionHorizoneTimeLimit = camera_get_view_width(view_camera[0])/transitionSpd;
	transitionVerticalTimeLimit = camera_get_view_height(view_camera[0])/transitionSpd;
	chargeCounter = 0;
	atkCounter = 0;
	atkCounterMax = 30;
	chargeLv1 = 30;
	chargeLv2 = 110;	

	//Vector Speed
	hsp = 0;
	ex_hsp = 0;
	vsp = 0;

	//Transition
	transition_type = noone;
	
	enum Transition{
		Up, Down, Left, Right, Door, BossAppear
	}
	
	enum Character {
		X,
		Zero
	}
	character = noone;
	
	enum AttackType {
		Slash,
		Shoot
	}
	
	enum State {
		SkillAttack,
	    StandAttack,
		RunAttack,
		DashAttack,
		Dash,
		Run,
		Stand,
		Jump,
		JumpAttack,
		Crouch,
		CrouchAttack,
		Climb,
		ClimbUp,
		ClimbAttack,
		WallSlide,
		WallSlideAttack,
		WallJump,
		Fly,
		FlyAttack,
		Hit
	}
	
	spriteGround = noone;
	spriteAttack = noone;
	spriteAir = noone;
	spriteDamage = noone;
	state = State.Stand;
	previous_state = State.Stand;
	
	//Saber
	sword = instance_create_depth(x,y, 0, objSaber);
	slashState = 0;	
	
	//SFX
	canPlaySFX[0] = true; //Charge Lv 0
	canPlaySFX[1] = true; //Charge Lv 2
	canPlaySFX[2] = true; //Landing on wall or ground or any solid object
	canPlaySFX[3] = true; //Teleport in
	canPlaySFX[4] = true; //Dash
	canPlaySFX[5] = true; //Lading on Moving Platform
	canPlaySFX[6] = true; //Wall jump
	canPlaySFX[7] = true; //Victory Pose
	
	
	//Weapons
	global.weaponIndex = 0;
	for (var i = 0; i < 9; ++i) {
		global.weaponUnlocked[i] = true;
	}
	switch (object_index) {
	    case objX:		
			enum ArmorType {
				Alpha, // Abilities: Air Dash, Take less damage, Big shot buster that create an explosion, Giga attack : Nova Strike
				Delta, // Abilities:  
				Beta, // Abilities: Double Jump, Shadow Dash and Air Dash, rapid shot. Giga attack : X Override
				Normal // Abilities: like X in Mgm x4
			}
			armor_type = ArmorType.Beta;
			suit_type = noone;
			
			//Character code
			character = Character.X;
			//Charging
			aura = instance_create_depth(x,y, -1, objPlayerCharge);

			//Mask
			maskCrouch = spr_x_crouch_mask;
			maskNormal = spr_x_mask;
			maskDash = spr_x_dash_mask;
			
			//Weapon Sprite
			sprBusterLv[0] = spr_x_buster_lv1;
			sprBusterLv[1] = spr_x_buster_lv2;
			
			//Color Palete
			colorMatch[0] = new Color(0,128,248);
			colorMatch[1] = new Color(0,64,240);
			colorMatch[2] = new Color(32,48,128);	
			colorMatch[3] = new Color(120,216,240);
			colorMatch[4] = new Color(80,160,240);
			colorMatch[5] = new Color(24,88,176);
					
			//Classic Color
			colorReplace[0] = new Color(0,128,248);
			colorReplace[1] = new Color(0,64,240);
			colorReplace[2] = new Color(32,48,128);	
			colorReplace[3] = new Color(120,216,240);
			colorReplace[4] = new Color(80,160,240);
			colorReplace[5] = new Color(24,88,176);

			switch (armor_type) {
			    case ArmorType.Normal:
			       //Non-Attack Sprite
				    
					sprVictoryPose = spr_x_victory;
					sprRecall = spr_x_recall;
					sprTeleport = spr_x_transform;
					sprStand = spr_x_idle;
					sprRun = spr_x_run;
					sprClimb = spr_x_climb;
					sprClimbUp = spr_x_climb_up;
					sprJump[0] = spr_x_jump_0; 
					sprJump[1] = spr_x_jump_1; 
					sprDash = spr_x_dash;
					sprWallSlide = spr_x_wall_slide;
					sprWallJump = spr_x_wall_jump;
					sprCrouch = spr_x_crouch;
					sprHit = spr_x_hard_hit;			
					sprDead = spr_x_dead;

					//Attack Sprite
					sprStandAtk[0] = spr_x_stand_shoot;
					sprStandAtk[1] = spr_x_stand_shoot;
					sprStandAtk[2] = spr_x_stand_shoot;
					sprRunAtk = spr_x_run_shoot;
					sprClimbAtk = spr_x_climb_shoot;
					sprJumpAtk[0] = spr_x_jump_shoot_0; 
					sprJumpAtk[1] = spr_x_jump_shoot_1; 
					sprDashAtk = spr_x_dash_shoot;
					sprCrouchAtk = spr_x_crouch_shoot;
					sprWallSlideAtk = spr_x_wall_slide_shoot;									
					sprBusterLv[2] = spr_x_buster_lv3;					
					
			        break;
					
				case ArmorType.Beta:
					
					pushBackCounterMax = 10;
					hitCounterMax = 120;
					
					sprVictoryPose = spr_ba_victory;
					sprRecall = spr_x_recall;
					sprTeleport = spr_x_transform;
					sprStand = spr_ba_idle;
					sprRun = spr_ba_run;
					sprClimb = spr_ba_climb;
					sprClimbUp = spr_ba_climb_up;
					sprJump[0] = spr_ba_jump_0; 
					sprJump[1] = spr_ba_jump_1; 
					sprDash = spr_ba_dash;
					sprWallSlide = spr_ba_wall_slide;
					sprWallJump = spr_ba_wall_jump;
					sprCrouch = spr_ba_crouch;
					sprHit = spr_ba_hard_hit;			
					sprDead = spr_ba_dead;

					//Attack Sprite
					sprStandAtk[0] = spr_ba_stand_shoot;
					sprStandAtk[1] = spr_ba_stand_shoot;
					sprStandAtk[2] = spr_ba_stand_shoot;
					sprRunAtk = spr_ba_run_shoot;
					sprClimbAtk = spr_ba_climb_shoot;
					sprJumpAtk[0] = spr_ba_jump_shoot_0; 
					sprJumpAtk[1] = spr_ba_jump_shoot_1; 
					sprDashAtk = spr_ba_dash_shoot;
					sprCrouchAtk = spr_ba_crouch_shoot;
					sprWallSlideAtk = spr_ba_wall_slide_shoot;	
					sprBusterLv[0] = spr_ba_buster_lv1;
					sprBusterLv[1] = spr_ba_buster_lv2;
					sprBusterLv[2] = spr_ba_buster_lv3;		
					
					//Skill
					sprGigaAtk[0] = spr_ba_override;
					
					
					colorMatch[3] = new Color(120,216,240);
					colorMatch[4] = new Color(80,160,240);
					colorMatch[5] = new Color(24,88,176);
					//colorMatch[6] = new Color(240,64,16);
					//colorMatch[7] = new Color(128,64,32);	
										
					colorReplace[3] = colorReplace[0];
					colorReplace[4] = colorReplace[1];
					colorReplace[5] = colorReplace[2];				
					//colorReplace[6] = new Color(240,240,240);
					//colorReplace[7] = new Color(128,128,128);	
					break;
				
				case ArmorType.Delta:
					
					
					sprVictoryPose = spr_da_victory;
					sprRecall = spr_x_recall;
					sprTeleport = spr_x_transform;
					sprStand = spr_da_idle;
					sprRun = spr_da_run;
					sprClimb = spr_da_climb;
					sprClimbUp = spr_da_climb_up;
					sprJump[0] = spr_da_jump_0; 
					sprJump[1] = spr_da_jump_1; 
					sprDash = spr_da_dash;
					sprWallSlide = spr_da_wall_slide;
					sprWallJump = spr_da_wall_jump;
					sprCrouch = spr_da_crouch;
					sprHit = spr_da_hard_hit;			
					sprDead = spr_da_dead;

					//Attack Sprite
					sprStandAtk[0] = spr_da_stand_shoot;
					sprStandAtk[1] = spr_da_stand_shoot;
					sprStandAtk[2] = spr_da_stand_shoot;
					sprRunAtk = spr_da_run_shoot;
					sprClimbAtk = spr_da_climb_shoot;
					sprJumpAtk[0] = spr_da_jump_shoot_0; 
					sprJumpAtk[1] = spr_da_jump_shoot_1; 
					sprDashAtk = spr_da_dash_shoot;
					sprCrouchAtk = spr_da_crouch_shoot;
					sprWallSlideAtk = spr_da_wall_slide_shoot;									
					sprBusterLv[2] = spr_ma_buster_lv3;		
					
					//Skill
					
					
					colorMatch[3] = new Color(120,216,240);
					colorMatch[4] = new Color(80,160,240);
					colorMatch[5] = new Color(24,88,176);
					colorMatch[6] = new Color(240,64,16);
					colorMatch[7] = new Color(128,64,32);	
										
					colorReplace[3] = colorReplace[0];
					colorReplace[4] = colorReplace[1];
					colorReplace[5] = colorReplace[2];				
					colorReplace[6] = new Color(240,240,240);
					colorReplace[7] = new Color(128,128,128);	
					
					break;
				
				case ArmorType.Alpha:
					//Non-Attack Sprite
				  
					
					sprVictoryPose = spr_ma_victory;
					sprRecall = spr_x_recall;
					sprTeleport = spr_x_transform;
					sprStand = spr_ma_idle;
					sprRun = spr_ma_run;
					sprClimb = spr_ma_climb;
					sprClimbUp = spr_ma_climb_up;
					sprJump[0] = spr_ma_jump_0; 
					sprJump[1] = spr_ma_jump_1; 
					sprDash = spr_ma_dash;
					sprWallSlide = spr_ma_wall_slide;
					sprWallJump = spr_ma_wall_jump;
					sprCrouch = spr_ma_crouch;
					sprHit = spr_ma_hard_hit;			
					sprDead = spr_ma_dead;

					//Attack Sprite
					sprStandAtk[0] = spr_ma_stand_shoot;
					sprStandAtk[1] = spr_ma_stand_shoot;
					sprStandAtk[2] = spr_ma_stand_shoot;
					sprRunAtk = spr_ma_run_shoot;
					sprClimbAtk = spr_ma_climb_shoot;
					sprJumpAtk[0] = spr_ma_jump_shoot_0; 
					sprJumpAtk[1] = spr_ma_jump_shoot_1; 
					sprDashAtk = spr_ma_dash_shoot;
					sprCrouchAtk = spr_ma_crouch_shoot;
					sprWallSlideAtk = spr_ma_wall_slide_shoot;	
					
					sprBusterLv[1] = spr_ma_buster_lv2;
					sprBusterLv[2] = spr_ma_buster_lv3;	
					
					//Skill
					sprGigaAtk[0] = spr_ma_nova_strike_0;
					sprGigaAtk[1] = spr_ma_nova_strike_1;					
					
					colorMatch[6] = new Color(224,224,224);
					colorMatch[7] = new Color(152,152,152);	
					colorMatch[8] = new Color(240,64,16);
					colorMatch[9] = new Color(128,64,32);	
										
					colorReplace[6] = new Color(232,224,64);
					colorReplace[7] = new Color(248,176,128);					
					colorReplace[8] = new Color(41,82,132);
					colorReplace[9] = new Color(24,57,90);		
					break;
					
			    default:			        
			        break;
			}			
			
			shoot_height = 0;
			shoot_width = 0;

			PlayerCameraInit();
			PlayerCamera();

	        break;
			
		case objZ:
			//Character code
			character = Character.Zero;
			//Charging
			aura = instance_create_depth(x,y, -1, objPlayerCharge);
			//Suit type
			enum SuitType {
				Normal,
				Virus,
				Black
			}
			suit_type = SuitType.Black;
			armor_type = noone;
			saber_style = 0;

			//Mask
			maskCrouch = spr_z_crouch_mask;
			maskNormal = spr_z_mask;
			maskDash = spr_z_dash_mask;

			//Non-Attack Sprite
			sprVictoryPose = spr_z_victory;
			sprRecall = spr_z_recall;
			sprTeleport = spr_z_transform;
			sprStand = spr_z_stand;
			sprRun = spr_z_run;
			sprClimb = spr_z_climb;
			sprClimbUp = spr_z_climb_up;
			sprJump[0] = spr_z_jump_0; 
			sprJump[1] = spr_z_jump_1; 
			sprJump[2] = spr_z_jump_2;
			sprDash = spr_z_dash;
			sprWallSlide = spr_z_wall_slide;
			sprWallJump = spr_z_wall_jump;
			sprHit = spr_z_hard_hit;
			sprCrouch = spr_z_crouch;
			sprDead = spr_z_dead;

			
			if(saber_style == 0){
				sprStandAtk[0] = spr_z_stand_slash_4;
				sprStandAtk[1] = spr_z_stand_slash_5;
				sprStandAtk[2] = spr_z_stand_slash_6;
				//sprStandAtk[3] = spr_z_stand_slash_3;
				sprStandAtkMask[0] = spr_z_stand_slash_mask_4;
				sprStandAtkMask[1] = spr_z_stand_slash_mask_5;
				sprStandAtkMask[2] = spr_z_stand_slash_mask_6;
				//sprStandAtkMask[3] = spr_z_stand_slash_mask_3;
				
				sprJumpAtk[0] = spr_z_jump_slash_0;
				sprJumpAtk[1] = spr_z_jump_slash_0;
				sprJumpAtkMask = spr_z_jump_slash_mask_0;
				
				sprWallSlideAtk = spr_z_wall_slide_slash_0;
				sprWallSlideAtkMask = spr_z_wall_slide_slash_mask_0;
				
				sprClimbAtk = spr_z_climb_slash_0;
				sprClimbAtkMask = spr_z_climb_slash_mask_0;
				
				sprRollAtk = spr_z_roll_slash_0;
				sprRollAtkMask = spr_z_roll_slash_mask_0;
				
				sprCrouchAtk = spr_z_crouch_slash_0;
				sprCrouchAtkMask = spr_z_crouch_slash_mask_0;
			}else{
				sprStandAtk[0] = spr_z_stand_slash_0;
				sprStandAtk[1] = spr_z_stand_slash_1;
				sprStandAtk[2] = spr_z_stand_slash_2;
				sprStandAtk[3] = spr_z_stand_slash_3;
				sprStandAtkMask[0] = spr_z_stand_slash_mask_0;
				sprStandAtkMask[1] = spr_z_stand_slash_mask_1;
				sprStandAtkMask[2] = spr_z_stand_slash_mask_2;
				sprStandAtkMask[3] = spr_z_stand_slash_mask_3;
				
				sprJumpAtk[0] = spr_z_jump_slash;
				sprJumpAtk[1] = spr_z_jump_slash;
				sprJumpAtkMask = spr_z_jump_slash_mask;
				
				sprWallSlideAtk = spr_z_wall_slide_slash;
				sprWallSlideAtkMask = spr_z_wall_slide_slash_mask;
				
				sprClimbAtk = spr_z_climb_slash;
				sprClimbAtkMask = spr_z_climb_slash_mask;
				
				sprRollAtk = spr_z_roll_slash;
				sprRollAtkMask = spr_z_roll_slash_mask;
				
				sprCrouchAtk = spr_z_crouch_slash;
				sprCrouchAtkMask = spr_z_crouch_slash_mask;
			}

			sprRunAtk = spr_z_stand_slash_0; 
			sprDashAtk = spr_z_stand_slash_0;

			
			
			//Skills
			sprGigaAtk = spr_z_giga_atk;
			sprUpperCut = spr_z_skill_thunder_uppercut;
			sprUpperCutMask = spr_z_skill_thunder_uppercut_mask;
			sprTornadoSlash = spr_z_slash_tornado;
			sprTornadoSlashMask = spr_z_slash_tornado_mask;
			sprDashSlash = spr_z_dash_slash;			
			sprDashSlashMask = spr_z_dash_slash_mask;
			
			
			///original Color Palete	
			//Primary color
			colorMatch[0] = new Color(224,32,0);
			colorMatch[1] = new Color(224,32,0);
			colorMatch[2] = new Color(160,32,0);
			colorMatch[3] = new Color(96,32,0);
			
			
			switch (suit_type) {		
				case SuitType.Normal:
					slashStateMax = 2;
					//Primary color
					colorReplace[0] = new Color(224,32,0);
					colorReplace[1] = new Color(224,32,0);
					colorReplace[2] = new Color(160,32,0);
					colorReplace[3] = new Color(96,32,0);
					
					break;
				
				case SuitType.Black:
					slashStateMax = 2;
			        colorReplace[0] = new Color(72,72,72);
					colorReplace[1] = new Color(72,72,72);
					colorReplace[2] = new Color(52,52,52);
					colorReplace[3] = new Color(32,32,32);	
					
					
				    break;
					
				case SuitType.Virus:
					slashStateMax = 3;
			        colorReplace[0] = new Color(132,0,132);
			        colorReplace[1] = new Color(132,0,132);
					colorReplace[2] = new Color(102,0,102);
					colorReplace[3] = new Color(62,0,62);
					
				    break;
			}					
			
			
			PlayerCameraInit();
			PlayerCamera();
			break;
	}	
	
	color_palete_lenght = array_length(colorReplace);
	for (var i = 0; i < color_palete_lenght; ++i) {
		sh_handle_match[i] = shader_get_uniform(sh_color_replace, "colorMatch"+string(i));
		sh_handle_replace[i] = shader_get_uniform(sh_color_replace, "colorReplace"+string(i));    
	}
	//Initial posistion in the stage beginning
	y = get_view_y() - 224;
}