// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BossSimpleJump(distance, height, dir){
	vsp = -sqrt(abs(2*grav*height));
	jumpCounter = ceil(abs(2*vsp/grav))+5;
	jumpCounterHalf = ceil(jumpCounter/2);
	hsp = (distance/jumpCounter)*dir;	
	play_sfx(-1, sfx_boss_jump);
	on_ground = false;
	set_sprite(sprJump[0]);
}