// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerBeforeDefeatBoss(){
	if (isBlocked or global.isPaused) and !isMissionComplete{ 
		image_alpha = 1; 
		PlayerFallWhenFreeze();
	}
}