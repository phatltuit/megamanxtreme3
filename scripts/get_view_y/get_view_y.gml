// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function get_view_y(){
	return camera_get_view_y(view_camera[0]);
}