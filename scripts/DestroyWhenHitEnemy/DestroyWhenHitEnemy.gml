// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function DestroyWhenHitEnemy(){
	var enemy = instance_place(x, y, objEnemy) 
	if enemy >= 0 and !isHitEnemy{
		alarm[0] = 3;
		isHitEnemy = true;
	}
}