// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ContinueAction(sprite_hit, sprite_continue, prev_img_index){
	if sprite_is(sprite_hit) {
		set_sprite(sprite_continue);
		image_index = prev_img_index;
	}
}