// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function RushToPlayer(dist, spd, delay_time, next_phase){
	if !phaseStart {
		face_to_player();		
		set_sprite(sprRush);
		var rushSpd = abs(spd);					
		hsp = rushSpd*image_xscale;
		rushCounter = dist/rushSpd;
		phaseStart = true;
	}
	prev_image_index = image_index;
				
	if rushCounter > 0 {					
		ContinueAction(sprHit, sprRush, prev_image_index);
		rushCounter--;	
	}else {
		set_sprite(sprStand);
		hsp = 0;
		WaitForNextPhase(delay_time, next_phase);
	}
}