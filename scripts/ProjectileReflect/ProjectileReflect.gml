// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ProjectileReflect(){
	if tilemap_get_at_pixel(tilemap, bbox_right + hspeed,bbox_bottom) != 0 
		or tilemap_get_at_pixel(tilemap, bbox_right + hspeed,bbox_top) != 0 {
		hspeed = -hspeed;
	}else if tilemap_get_at_pixel(tilemap, bbox_left + hspeed,bbox_bottom) != 0 
		or tilemap_get_at_pixel(tilemap, bbox_left + hspeed,bbox_top) != 0 {
		hspeed = -hspeed;
	}
		
	if tilemap_get_at_pixel(tilemap, bbox_right,bbox_bottom + vspeed) != 0 
		or tilemap_get_at_pixel(tilemap, bbox_left + hspeed,bbox_bottom + vspeed) != 0 {
		vspeed = -vspeed;
	}else if tilemap_get_at_pixel(tilemap, bbox_right,bbox_top + vspeed) != 0 
		or tilemap_get_at_pixel(tilemap, bbox_left,bbox_top + vspeed) != 0 {
		vspeed = -vspeed;
	}	
}