function PlayerCamera() {
	camera = view_camera[0];
	cameraWidth = camera_get_view_width(camera);
	cameraHeight = camera_get_view_height(camera);
	cameraCenterX = round(cameraWidth/2);
	cameraCenterY = round(cameraHeight/2);
	cameraX = x - cameraCenterX;
	cameraY = y - cameraCenterY;	
		
	camera_set_view_pos(camera, cameraX, cameraY);
	//Stop at zone borders
	if cameraX > sectionRight-cameraWidth{
		cameraX = sectionRight-cameraWidth;
	}
	if cameraX < sectionLeft{			
		cameraX = sectionLeft;
	}

	
	if cameraY > sectionBottom - cameraHeight{
		cameraY = sectionBottom - cameraHeight;	
	}
	if cameraY < sectionTop{
		cameraY = sectionTop;
	}
	
	camera_set_view_pos(camera, cameraX, cameraY);	
	
	if collision_rectangle_list(bbox_left, bbox_top, bbox_right, bbox_bottom, objViewBoudary,false, true, view_collided, false) > 1 {			
		currentView = ds_list_find_value(view_collided, 0);
		
		if currentView.bbox_right <= bbox_right-4 and currentView.bbox_right > bbox_left{
			transition_type = Transition.Right;
			isFreeze = true;
		}else if currentView.bbox_left >= bbox_left + 4 and currentView.bbox_left < bbox_right{
			transition_type = Transition.Left;
			isFreeze = true;
		}else if currentView.bbox_bottom <= bbox_bottom-16 and currentView.bbox_bottom > bbox_top{
			transition_type = Transition.Down;
			isFreeze = true;
		}else if currentView.bbox_top >= bbox_top+16 and currentView.bbox_top < bbox_bottom{
			transition_type = Transition.Up;
			isFreeze = true;
		}
	}
}
