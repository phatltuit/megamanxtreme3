// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerSkillsAttack(){	
	if wp < 0 {
		wp = 0;	
	}
	if is_Zero(){	
		if !isAtk and !isPushBack {
			//Zero giga attack
			if key_giga_attack and on_ground and !isPushBack{
				if !isSkillAtk {
					isSkillAtk = true;						
					image_index = 0;
					state = State.SkillAttack;
					spriteAttack = sprGigaAtk;
					global.weaponIndex = 8;
					canHit = false;
				}
			}
			
			if key_up and key_skill_attack and !isSkillAtk and on_ground{
				isSkillAtk = true;	
				image_index = 0;
				state = State.SkillAttack;
				spriteAttack = sprUpperCut;			
				global.weaponIndex = 1;
				set_sprite_of(sword, sprUpperCutMask);
				canHit = false;
			}else if key_skill_attack and !isSkillAtk and on_ground and !isDash {
				isSkillAtk = true;	
				image_index = 0;
				state = State.SkillAttack;
				spriteAttack = sprTornadoSlash;	
				set_sprite_of(sword, sprTornadoSlashMask);
				global.weaponIndex = 2;		
			}else if isDash and !isSkillAtk and key_skill_attack {
				isSkillAtk = true;	
				image_index = 0;
				state = State.SkillAttack;
				spriteAttack = sprDashSlash;	
				set_sprite_of(sword, sprDashSlashMask);	
				global.weaponIndex = 3;		
				dashCounter = 0;
				isDash = false;
				hsp = dashSpd*image_xscale;
				canDash = false;
			}
			
			//Skills
			if isSkillAtk {
				switch (global.weaponIndex) {
					case 1:
						key_left = 0;
						key_right = 0;
						if sub_img_at(5) and on_ground{							
							vsp = -jumpSpd-2;								
							on_ground = false;
							wp-=2;
						}
						
						if !on_ground {
							skillCounter++;
							hsp = image_xscale;
							if skillCounter mod 4 == 0 and image_index >= 5 and image_index < 9{
								var atk = instance_create_depth(x+image_xscale*18, y + 30, depth - 1, objPlayerProjectile);
								set_sprite_of(atk, spr_z_thunder_uppercut_eff);	
								atk.damage = 2;
							}
							
							if floor(image_index) >= 8 and skillCounter < skillCounterMax/2{
								image_index = 5;	
							}
					
							if skillCounter >= skillCounterMax/2 {
								if skillCounter == skillCounterMax/2 {
									canHit = true;
									image_index = 9;	
								}
								if animation_end(){
									skillCounter = 0;
									isSkillAtk = false;
									vsp = 0;
									global.weaponIndex = 0;
									set_sprite_of(sword, noone);
								}
							}
						}
						break;
						
					case 2:
						key_left = 0;
						key_right = 0;
						if on_ground {
							if image_index + image_speed == 4 {
								var atk = instance_create_depth(x+image_xscale*50, y-16, depth - 1, objPlayerProjectile);
								set_sprite_of(atk, spr_z_skill_tornado);								
								atk.damage = 3;
								atk.image_xscale = image_xscale;
								atk.hsp = 4*image_xscale;				
							}
							
							if animation_end() {
								isSkillAtk = false;	
								global.weaponIndex = 0;
								set_sprite_of(sword, noone);
							}
						}
						break;
						
					case 3:						
						if animation_end() {
							isSkillAtk = false;	
							global.weaponIndex = 0;
							set_sprite_of(sword, noone);
							grav = 0.25;
						}else{
							hsp = image_xscale*runSpd/2*((image_xscale == 1 and canGoRight) or (image_xscale == -1 and canGoLeft));	
							vsp = 0;
							grav = 0;
						}
						break;
					
					case 8:
						hsp = 0;
						if image_index + image_speed == 7 and instance_number(objPlayerProjectile) < 5{
							var i = 0;
							repeat 9 {
								var atk = instance_create_depth(x, y, depth-1,objPlayerProjectile);
								set_sprite_of(atk, spr_z_skill_sonic_burst);	
								atk.speed = 5;
								atk.damage = 5;
								atk.alarm[0] = 240;
								atk.direction = i;
								atk.image_angle = atk.direction+180;
								i+=22.5;
							}	
							wp-=4;
							//Sound
						}
						if animation_end() {
							isSkillAtk = false;		
							global.weaponIndex = 0;
							canHit = true;
						}
					    break;
						
					
					default:
					    // code here
					    break;
				}
			}
		}
	}else{
		if !isAtk{
		//Only giga attack of X has animation
			if isOverride {				
				if wp > 0 {		
					skillCounter++;
					wp -= (skillCounter mod 30 == 0);
				}else{
					isOverride = 0;
					skillCounter = 0;
				}
			}
			PlayerPhysics();
			if key_giga_attack and !isPushBack and !isWallSlide{
				switch (armor_type) {
					case ArmorType.Beta:					
						if !isSkillAtk and !isOverride{
							isSkillAtk = true;	
							image_index = 0;
							state = State.SkillAttack;
							spriteAttack = sprGigaAtk[0];
							global.weaponIndex = 0;
							canHit = false;							
						}
						if isSkillAtk {
							key_left = 0;
							key_right = 0;
							vsp = 0;
							grav = 0;
							if sprite_is(sprGigaAtk[0]) and animation_end(){
								isOverride = true;
								isSkillAtk = false;
								canHit = true;
								grav = 0.25;
							}	
						}						
						break;
				
				    case ArmorType.Alpha:	
						if !isSkillAtk {
							isSkillAtk = true;	
							image_index = 0;
							state = State.SkillAttack;
							spriteAttack = sprGigaAtk[0];
							global.weaponIndex = 0;
							canHit = false;
							dashCounter = 0;
						}
						if isSkillAtk {
							key_left = 0;
							key_right = 0;
							if sprite_is(sprGigaAtk[0]){
								vsp = -0.4;					
								grav = 0;
								hsp = image_xscale*runSpd;
								if animation_end() {
									spriteAttack = sprGigaAtk[1];	
								}							
							}
							if sprite_is(sprGigaAtk[1]){
								if instance_number(objPlayerProjectile) < 1{
									var atk = instance_create_depth(x,y, depth, objPlayerProjectile);
									atk.image_xscale = image_xscale;
									atk.visible = false;
									atk.damage = 5;
									set_sprite_of(atk, sprGigaAtk[1]);
									atk.alarm[0] = dashCounterMax;
									//Sound
								}							
								hsp = image_xscale*dashSpd*3;
								grav = 0;
								canDash = false;								
								dashCounter++							
							}
							
							if dashCounter >= dashCounterMax/2
							or tilemap_get_at_pixel(tilemap, bbox_horizone + hsp+sign(hsp), y) != 0 
							or tilemap_get_at_pixel(tilemap, bbox_horizone + hsp+sign(hsp), bbox_top) != 0
							or tilemap_get_at_pixel(tilemap, bbox_horizone + hsp+sign(hsp), bbox_bottom) != 0{
								isSkillAtk = false;	
								dashCounter = 0;
								hsp = 0;
								canDash = false;
								grav = 0.25;
								canHit = true;
							}
						}
				
				        break;
				    default:
				        // code here
				        break;
				}
			}
		}
			
	}
}