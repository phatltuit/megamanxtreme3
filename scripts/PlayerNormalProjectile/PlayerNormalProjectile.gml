// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerNormalProjectile(bullet_sprite, X, Y, xscale, spd, objProjectile){
	var shot  = instance_create_depth(X,Y, depth-1,objProjectile);
	shot.image_xscale = xscale;
	shot.hspeed = spd;
	shot.sprite_index = bullet_sprite;
}

function PlayerSpecialProjectile(bullet_sprite, X, Y, dir, spd, objProjectile) {
	var shot  = instance_create_depth(X,Y, depth-1,objProjectile);
	//shot.image_xscale = xscale;
	shot.speed = spd;
	shot.sprite_index = bullet_sprite;
	shot.direction = dir;
	shot.image_angle = dir+180;
}