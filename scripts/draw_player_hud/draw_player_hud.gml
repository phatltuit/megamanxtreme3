// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function draw_player_hud(){
	_x_corner = get_view_x()+8;
	_x_start_lid = _x_corner + 35;
	_x_start = _x_start_lid + 1;
	_y_corner = get_view_y()+8;
	_hud_icon = is_Zero()?spr_z_hud_hp_header:spr_x_hud_hp_header;	
	draw_sprite(_hud_icon, 0, _x_corner, _y_corner);
	draw_set_font(MessageText);
	draw_text(_x_corner+23.5, _y_corner+2.5, life);
	draw_sprite_ext(spr_player_hud_lid, 0, _x_start_lid, _y_corner, 1,1,0, c_white, 1);	
	for (var i = 1; i <= maxHp; ++i) {	
		draw_sprite(spr_player_hud_part, 0, _x_start + i*2, _y_corner);	
		if isHit and i <= prev_hp and i > hp {
			draw_sprite(spr_player_hud_hp_loss, 0, _x_start + i*2, _y_corner);
		}else{
			draw_sprite(spr_player_hud_empty_hp, 0, _x_start + i*2, _y_corner);
		}
	}
	draw_sprite_ext(spr_player_hud_lid, 0, _x_start_lid+maxHp*2+7, _y_corner, -1,1,0, c_white, 1);
	
	for (var i = 1; i <= hp; ++i) {	
		draw_sprite(spr_player_hud_hp, 0, _x_start + i*2, _y_corner);
	}
	
	_hud_energy_icon = spr_hud_energy_header;
	_xx_corner = _x_corner;
	_xx_start_lid = _xx_corner + 35;
	_xx_start = _xx_start_lid + 1;
	_yy_corner = _y_corner + 16;
	draw_sprite(_hud_energy_icon, 0, _xx_corner, _yy_corner);
	draw_sprite_ext(spr_player_hud_lid, 0, _xx_start_lid, _yy_corner, 1,1,0, c_white, 1);	
	for (var i = 1; i <= maxWp; ++i) {	
		draw_sprite(spr_player_hud_part, 0, _xx_start + i*2, _yy_corner);			
		draw_sprite(spr_player_hud_empty_hp, 0, _xx_start + i*2, _yy_corner);
	}
	for (var i = 1; i <= wp; ++i) {	
		
		draw_sprite(spr_player_hud_mp, 0, _xx_start + i*2, _yy_corner);
		if is_Zero() {
			draw_sprite_ext(spr_player_hud_mp_color, 0, _xx_start + i*2, _yy_corner, 1,1,0, c_red,1);
		}else{
			var color = colorReplace[0];
			draw_sprite_ext(spr_player_hud_mp_color, 0, _xx_start + i*2, _yy_corner, 1,1,0, make_color_rgb(color.red,color.green,color.blue),1);
		}
	}
	draw_sprite_ext(spr_player_hud_lid, 0, _xx_start_lid+maxHp*2+7, _yy_corner, -1,1,0, c_white, 1);
}