// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerSideCollision(){
	for (var i = 0; i < bbox_bottom-bbox_top; ++i) {
	     if tilemap_get_at_pixel(tilemap, bbox_horizone + hsp, bbox_top + i) != 0{
			 return true;
		 }
	}
	return false;
}