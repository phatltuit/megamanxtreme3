// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerTeleport(){	
	if beginCounter < beginCounterMax{
		beginCounter++;
		switch (beginCounter/2 mod 3) {
		    case 0:
		        image_alpha = 0;
		        break;
				
			case 1:
		        image_alpha = 0;
		        break;
				
			case 2:
		        image_alpha = 1;
		        break;
		    default:
		        // code here
		        break;
		}
	}else{	
		if y < ystart{
			image_alpha = 1;
			if vsp == 0{
				set_sprite(sprTeleport);
				vsp = jumpSpd;
			}
			if vsp != 0{
				image_index = 0;	
			}
			play_sfx(3, sfx_teleport_in);
		}else{
			vsp = 0;
			y = ystart;
			if animation_end(){
				set_sprite(sprStand);			
				isReady = true;
			}
		}	
	}
}