// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function in_view(object){
	if object != noone {
		return object.bbox_left >= get_view_x()
		and object.bbox_right <= get_view_x() + get_view_w()
		and object.bbox_top >= get_view_y()
		and object.bbox_bottom <= get_view_y() + get_view_h();
	}
	return bbox_left >= get_view_x()
		and bbox_right <= get_view_x() + get_view_w()
		and bbox_top >= get_view_y()
		and bbox_bottom <= get_view_y() + get_view_h();
}