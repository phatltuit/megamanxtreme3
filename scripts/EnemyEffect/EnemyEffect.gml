// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function EnemyEffect(sprite, xscale,X,Y){
	if instance_number(objEnemyEffect) <= 50{
		var hit = instance_create_depth(X, Y, -100, objEnemyEffect);
		set_sprite_of(hit, sprite);
		hit.image_xscale = xscale;
	}
	
}	