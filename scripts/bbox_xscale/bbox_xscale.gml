// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function bbox_xscale(){
	return image_xscale == 1?bbox_right:bbox_left;
}