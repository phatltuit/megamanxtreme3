// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function CanPushBack(weapon_sprite){	
	switch (weapon_sprite) {
	    case spr_z_stand_slash_mask_2:
			pushBackCounterMax = 40;
			if objZ.slashStateMax == 2 {
				return true;
			}
			return false;
			break;
		case spr_z_stand_slash_mask_3:
			pushBackCounterMax = 40;
			return true;
			break;
			
		case spr_z_jump_slash_mask:			
		case spr_z_roll_slash_mask:
			pushBackCounterMax = 30;
			return true;
			break;
		
		case spr_ma_buster_lv3:
			pushBackCounterMax = 15;
	        return true;
	        break;
		
	    default:
	        // code here
	        return false;
	}
}