// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BossBegin(){
	face_to_player();
	if !audio_is_playing(MusicBossBattle){
		global.music = MusicBossBattle;
		audio_play_sound(global.music, 0, true);	
	}
	set_sprite(sprPose);
	if animation_end(){
		frame_stop(image_number - 1)	
	}
    if hp < maxHp {
		beginCounter++;
		if beginCounter >= beginCounterMax {
			hp += 1;
			beginCounter = 0;
		}
	}else{			
		state = BossState.Active;	
		objPlayer.isBlocked = false;
		objPlayer.image_speed = 1;
	}
}