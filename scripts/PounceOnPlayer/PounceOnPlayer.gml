// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PounceOnPlayer(jump_dist, jump_height, delay_time, next_phase, isAtk){
	if !phaseStart{					
		face_to_player();
		if jump_dist > 0 {
			dist = jump_dist;
		}else{
			if instance_exists(objPlayer){
				dist = abs(objPlayer.x-x);	
				if dist <= 5 {
					dist = 5;	
				}
			}else{
				dist = 1;	
			}
		}
		BossSimpleJump( dist, jump_height, image_xscale);	
		phaseStart = true;
	}		
	prev_image_index = image_index;
	if !on_ground {
		if !isAtk{
			if vsp < 0 {			
				ContinueAction(spr_panther_got_hit, spr_panther_jump_0, prev_image_index);
				set_sprite(spr_panther_jump_0);				
				if animation_end() {
					frame_stop(image_number - 1)	
				}						
			}
			else{		
				if vsp >= 0 and vsp <= 0.5 {
					image_index = 0;	
				}
				set_sprite(spr_panther_pounce);	
				ContinueAction(spr_panther_got_hit, spr_panther_pounce, prev_image_index);
				if animation_end() {
					frame_stop(image_number - 1)	
				
				}
			}	
		}else{
			if vsp < 0 {			
				set_sprite(spr_panther_jump_0);		
				ContinueAction(spr_panther_got_hit, spr_panther_jump_0, prev_image_index);
				if animation_end() {
					frame_stop(image_number - 1)	
				}						
			}else{					
				if vsp >= 0 and vsp <= 0.5 {
					image_index = 0;	
				}
				if !isAttack and image_index + image_speed == 3 {
					isAttack = true;
					AirSonicSlash();
				}
				set_sprite(spr_panther_jump_atk);
				ContinueAction(spr_panther_got_hit, spr_panther_jump_atk, prev_image_index);
				if sprite_is(spr_panther_jump_atk) and animation_end(){		
					frame_stop(image_number - 1)					
				}					
			}		
		}
		
	}else{
		set_sprite(spr_panther_stand);	
	}
				
	if jumpCounter > 0 {
		jumpCounter--;	
	}else{
		hsp = 0;
		WaitForNextPhase(delay_time, next_phase);			
	}
}