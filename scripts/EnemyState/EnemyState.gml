// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function EnemyState(){
	switch (state) {
	    case EnemyState.Active:
			if !in_view(noone){
				isDead = true;	
				visible = false;
				state = EnemyState.DeadOutside;
			}else{
				isDead = false;
			}
	        break;
			
		case EnemyState.DeadOutside:
			if in_view(noone){
				activateCounter++;
				if activateCounter >= activateCounterMax{ 
					EnemyRespawn();
				}			
			}
			break;
			
		case EnemyState.DeadInside:
			if !in_view(noone) {
				state = EnemyState.DeadOutside;
			}else{
				visible = false;	
			}
			break;
			
	    default:
	        // code here
	        break;
	}
}