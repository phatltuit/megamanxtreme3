// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ColorClass(){
	//Color Change Replacement system
	Color = function(r,g,b) constructor {
		red = r;
		blue = b;
		green = g;
		
		static toShaderValue = function(value){
			return value/255;	
		}
	};
}