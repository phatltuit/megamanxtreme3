// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerDraw(){
	replace_color();	
	if beginCounter < beginCounterMax{
		draw_sprite_ext(spr_ready_txt, image_index,get_view_center_x(),get_view_center_y(),image_xscale, image_yscale, image_angle, image_blend, image_alpha);
	}else{
		draw_sprite_ext(sprite_index, image_index,x,y,image_xscale, image_yscale, image_angle, image_blend, image_alpha);
		draw_player_shadow();
		draw_player_hud();	
		draw_player_effect();
	}
	
	shader_reset();

}