// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerMove(){
	if !isPushBack {
		var ladderUp = instance_place(x, y, objLadder);
		var ladderDown = instance_place(x, y + 1, objLadder);
		
		if ladderUp < 0 and ladderDown < 0 {
			isClimb = false;
		}

		if ladderDown >= 0 and x <= ladderDown.x+6 and x >= ladderDown.x-6 and key_down == 1 and !isClimb 
		and (!tilemap_get_at_pixel(tilemap, x, bbox_bottom+1) and !tilemap_get_at_pixel(tilemap, bbox_left, bbox_bottom+1) and !tilemap_get_at_pixel(tilemap, bbox_right, bbox_bottom+1)){
			isClimb = true;
			isHyperJump = false;
			on_ground = false;
			hsp = 0;
			canDash = false;
			isDash = false;
			x = ladderDown.x;
			if instance_place(x, y, objTopLadder) >= 0 {
				y = instance_place(x, y, objTopLadder).bbox_top;		
			}		
		}else if ladderUp >= 0 and x <= ladderUp.x+6 and x >= ladderUp.x - 6 and key_up == 1 and !isClimb{
			isClimb = true;
			hsp = 0;
			x = ladderUp.x;	
			isHyperJump = false;
			canDash = false;
			isDash = false;
		}

		if !isClimb {				
			if key_down and on_ground {
				if is_Zero() and !isAtk{
					isCrouch = true;
				}else if !is_Zero(){
					isCrouch = true;	
				}
			}else{
				if !is_Zero(){
					isCrouch = false;		
				}else{
					if !isAtk {
						isCrouch = false;		
					}
				}
			}
								
			if isCrouch{
				mask_index = maskCrouch;
			}else if isDash {
				mask_index = maskDash;	
			}else{				
				mask_index = maskNormal;
			}
			canGoRight = tilemap_get_at_pixel(tilemap, bbox_right + hsp + image_xscale, bbox_top) == 0 
				and tilemap_get_at_pixel(tilemap, bbox_right + hsp + image_xscale, y) == 0 
				and tilemap_get_at_pixel(tilemap, bbox_right + hsp + image_xscale, bbox_bottom) == 0;
			canGoLeft = tilemap_get_at_pixel(tilemap, bbox_left + hsp + image_xscale, bbox_top) == 0 
				and tilemap_get_at_pixel(tilemap, bbox_left + hsp + image_xscale, y) == 0 
				and tilemap_get_at_pixel(tilemap, bbox_left + hsp + image_xscale, bbox_bottom) == 0;
			if on_ground and isDash{	
				hsp = (key_right*canGoRight - key_left*canGoLeft)*runSpd*(!isWallJump)*(!isDash)*(!isCrouch);	
			}else{
				hsp = (key_right - key_left)*runSpd*(!isWallJump)*(!isDash)*(!isCrouch);	
			}
		}else{		
			if key_up == 1 {		
				vsp = -(climbSpd)*(!isAtk);
				if instance_place(x, y, objLadder) < 0{
					isClimb = false;
				}
			}else if key_down == 1 {		
				vsp = climbSpd*(!isAtk);			
			}else{			
				vsp = 0;	
			}	
		}
		if !isWallJump {
			if is_Zero(){
				if state != State.StandAttack {
					image_xscale = key_right - key_left != 0 ? key_right - key_left : image_xscale;	
				}
			}else{				
				image_xscale = key_right - key_left != 0 ? key_right - key_left : image_xscale;					
			}
		}
	}else{
		hsp = -image_xscale*hitSpd;	
	}
}