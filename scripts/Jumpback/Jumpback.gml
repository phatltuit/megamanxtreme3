// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function Jumpback(isAtk, dist, height, dir, delay_time,next_phase){
	if !phaseStart {		
		BossSimpleJump(dist, height, dir);
		phaseStart = true;
		set_sprite(spr_panther_jump_back);
		image_index = 0;
	}
	prev_image_index = image_index;
	if vsp < 0 {
		set_sprite(spr_panther_jump_back);	
		if sprite_is(spr_panther_jump_back) and animation_end() {
			frame_stop(image_number - 1)	
		}
	}else{		
		set_sprite(spr_panther_roll);	
	}
	
	if isAtk{
		if ceil(jumpCounter) == ceil(jumpCounterHalf) {
			var i = (height >= 100) ? 45 : 0;
			repeat 4 {
				var atk = instance_create_depth(x, y, depth-100,objBossAtk);
				set_sprite_of(atk, spr_panther_sonic_boom_3_a);	
				atk.speed = 6;
				atk.alarm[0] = 180;
				atk.direction = i;
				atk.image_angle = i+180;
				i+=90;
			}		
		}
	}
	
	
	if jumpCounter > 0 {
		jumpCounter--;			
	}else{
		hsp = 0;
		set_sprite(spr_panther_stand);		
		WaitForNextPhase(delay_time,next_phase);	
	}
	
}