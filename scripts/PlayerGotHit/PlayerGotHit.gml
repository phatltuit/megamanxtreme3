
function PlayerGotHit(){
	if armor_type == ArmorType.Beta {
		if isDash {
			canHit = false;		
			image_alpha = 0;
		}else{
			canHit = true;	
			image_alpha = 1;
		}		
	}
	
	isXGetArmor = armor_type != noone and armor_type != ArmorType.Normal;
	isZBlack = suit_type == SuitType.Black;
	
	var enemy = instance_place(x,y, objEnemy);
	if enemy >= 0 and !enemy.isDead and !isHit and canHit{
		hp -= enemy.damage + isZBlack - isXGetArmor;
		isHit = true;
		isPushBack = true;
		if is_Zero(){
			sword.sprite_index = noone;			
		}
		vsp = 0;
		hsp = 0;
		isDash = false;
		grav = 0.25;
		dashCounter = 0;
		isAtk = false;
		atkCounter = 0;
		if isSkillAtk {
			isSkillAtk = false;
			skillCounter = 0;
		}
		audio_play_sound(sfx_damage, 0, false);	
	}
	
	var enemyAtk = instance_place(x,y, objEnemyAtk);
	if enemyAtk >= 0 and !isHit and canHit{
		hp -= enemyAtk.damage + isZBlack - isXGetArmor;
		isHit = true;
		isPushBack = true;
		if is_Zero(){
			sword.sprite_index = noone;			
		}
		vsp = 0;
		hsp = 0;
		isDash = false;
		grav = 0.25;
		dashCounter = 0;
		isAtk = false;
		atkCounter = 0;
		if isSkillAtk {
			isSkillAtk = false;
			skillCounter = 0;
		}
		audio_play_sound(sfx_damage, 0, false);		
	}
	
	if place_meeting(x, y, objDead){
		hp = 0;	
	}
	

	if isHit {
		audio_stop_sound(sfx_saber_slash);
		hitCounter++;	
		if hitCounter == pushBackCounterMax{
			isPushBack = false;				
		}
		
		switch (hitCounter/2 mod 3) {
		    case 0:
		        image_alpha = 1;
		        break;
				
			case 1:
		        image_alpha = 0.5;
		        break;
				
			case 2:
		        image_alpha = 0;
		        break;
		    default:
		        // code here
		        break;
		}	
		
		if hitCounter >= hitCounterMax {
			hitCounter = 0;
			image_alpha = 1;
			isHit = false;
			prev_hp = hp;
		}
	}
	
	if hp <= 0 {
		isDead = true;	
	}
}