// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerWallSide(){
	var side = image_xscale;
	for (var i = 0; i < bbox_bottom-bbox_top; ++i) {
	     if tilemap_get_at_pixel(tilemap, bbox_right + abs(hsp) + 2, bbox_top + i) != 0 {			 
			 return 1;
			 
		 }else if tilemap_get_at_pixel(tilemap, bbox_left - abs(hsp) - 2, bbox_top + i) != 0 {
			 return -1;
			 break;
		 }
	}
	return side;
}