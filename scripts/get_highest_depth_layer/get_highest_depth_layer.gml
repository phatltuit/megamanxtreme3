// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function get_highest_depth_layer(){
	all_layer = layer_get_all();
	highest_depth = layer_get_depth(all_layer[0]);
	for (var i = 0; i < array_length(all_layer); i++;){
		if layer_get_depth(all_layer[i]) < highest_depth {
			highest_depth = layer_get_depth(all_layer[i]);	
		}
	}
	return highest_depth;
}