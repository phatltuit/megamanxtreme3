// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerAttack(){
	#region Slash
		if !isSkillAtk {
			switch (character) {
			    case Character.Zero:
					{
				        if on_ground {
							if !isCrouch {
								if previous_state == State.JumpAttack{
									isAtk = false;
									sword.sprite_index = noone;
								}else{	
									if key_attack_release == 1 and (!isAtk or canCombo) and !isDash and slashState <= slashStateMax{								
										image_speed = slashSpd;	
										image_index = 0;
										sword.image_index = 0;
										if isAtk {
											slashState++;	
											slashState = min_max(slashState, 0, slashStateMax);
										}else{
											slashState = min_max(slashState, 0, slashStateMax);
										}									
										//sword.image_speed = slashSpd;
										canCombo = false;								
										isAtk = true;
										audio_stop_sound(sfx_saber_slash);
										play_sfx(-1,sfx_saber_slash)
									}
								
									if isAtk == true{
										hsp = 0;								
										if image_index + image_speed >= floor(image_number/2) and image_index + image_speed <= image_number and slashState <= slashStateMax-1{
											canCombo = true;									
										}else if image_index + image_speed > image_number{
											isAtk = false;	
											image_speed = 1;
											canCombo = false;
											slashState = 0;
											sword.sprite_index = noone;
										}else{
											canCombo = false;
										}
									}
								}
							}else{
								if key_attack_release == 1 and !isAtk {
									isAtk = true;		
									image_index = 0;	
									audio_stop_sound(sfx_saber_slash);
									play_sfx(-1,sfx_saber_slash);
								}	
							}
						}else{		
							if key_attack_release == 1 and !isAtk and !isDash{
								isAtk = true;	
								if key_up{
									isRollAtk = true;
								}
								image_index = 0;	
								audio_stop_sound(sfx_saber_slash);
								play_sfx(-1,sfx_saber_slash);
							}
						}	
					}
				
			        break;
				
				case Character.X:
					{				
						#region Shoot
							if isClimb{
								shoot_height = 12;
								shoot_width = image_xscale*16;
							}else if isDash {
								shoot_height = 7;
								shoot_width = image_xscale*31;
							}else {
								if on_ground {
									if isCrouch{
										shoot_height = 7;
										shoot_width = image_xscale*22;
									}else{
										if hsp == 0 {
											shoot_height = 11;	
											shoot_width = image_xscale*15;
										}else{
											shoot_height = 13;	
											shoot_width = image_xscale*17;
										}
									}
								}else{
									if !isWallSlide {
										if vsp < 0 {										
											shoot_height = 15;	
											shoot_width = image_xscale*14;
										}else{
											shoot_height = 14;	
											shoot_width = image_xscale*17;
										}
									}else{
										shoot_height = 12;	
										shoot_width = -image_xscale*18;
									}
								}
							}
							shootDir = (sprite_is(sprWallSlideAtk) || sprite_is(sprWallSlide) ? -image_xscale : image_xscale);
							if key_attack_hold == 1{
								isCharge = true;
								chargeCounter++;
								if chargeCounter >= chargeLv1 and chargeCounter < chargeLv2{								
									set_sprite_of(aura,spr_x_charge_aura_lv1);		
									play_sfx(0, sfx_charge_lv0);
								}else if chargeCounter > chargeLv2 { 								
									set_sprite_of(aura,spr_x_charge_aura_lv2);
									audio_stop_sound(sfx_charge_lv0);
									if canPlaySFX[1] {
										audio_play_sound(sfx_charge_lv2, 0, true);	
										canPlaySFX[1] = false;
									}
								}	
							}
							
							if key_attack_release == 1 and (atkCounter == 0 or atkCounter >= atkCounterMax/3) {	
								switch (global.weaponIndex) {
								    case 0:										
										audio_stop_sound(sfx_charge_lv2);
										if chargeCounter < chargeLv1{
											if instance_number(objPlayerProjectileLv1) < 3{
												PlayerNormalProjectile(sprBusterLv[0], x + shoot_width, y - shoot_height, shootDir, 7*shootDir,objPlayerProjectileLv1);	
												audio_play_sound(sfx_shoot_lv1, 0, false);
											}
										}else if chargeCounter >= chargeLv1	and chargeCounter < chargeLv2{
											if instance_number(objPlayerProjectileLv2) < 1{
												PlayerNormalProjectile(sprBusterLv[1], x + shoot_width, y - shoot_height, shootDir, 7*shootDir,objPlayerProjectileLv2);
												audio_play_sound(sfx_shoot_lv2, 0, false);
											}
										}else{
											if instance_number(objPlayerProjectileLv3) < 1{
												if armor_type == ArmorType.Beta {
													PlayerSpecialProjectile(sprBusterLv[2], x + shoot_width, y - shoot_height, shootDir == 1?45:135, 6, objPlayerProjectileLv3);
													PlayerNormalProjectile(sprBusterLv[2], x + shoot_width, y - shoot_height, shootDir, 6*shootDir,objPlayerProjectileLv3);
													PlayerSpecialProjectile(sprBusterLv[2], x + shoot_width, y - shoot_height, shootDir == 1?315:225, 6, objPlayerProjectileLv3);
													audio_play_sound(sfx_shoot_lv3, 0, false);
												}else{										
													PlayerNormalProjectile(sprBusterLv[2], x + shoot_width, y - shoot_height, shootDir, 7*shootDir,objPlayerProjectileLv3);
													audio_play_sound(sfx_shoot_lv3, 0, false);
												}	
											}
										}
								        break;
									
									case 8:
										if instance_number(objPlayerProjectileLv1) < 2 {
											var atk = instance_create_depth(x + shoot_width, y - shoot_height, depth - 1, objPlayerProjectileLv1);
											atk.hsp = 3.5 * shootDir;
											atk.image_xscale = shootDir;
											set_sprite_of(atk, spr_x_thunder_shock);
										}
										
										
										break;
								    default:
								        // code here
								        break;
								}
								isAtk = true;
								isCharge = false;							
								atkCounter = 1;
								chargeCounter = 0;
								set_sprite_of(aura,noone);								
								audio_stop_sound(sfx_charge_lv0);
								audio_stop_sound(sfx_charge_lv2);
								canPlaySFX[0] = true;
								canPlaySFX[1] = true;
									
							}	
							

							if isAtk{	
								if key_attack_release == 1 and hsp == 0 and !isDash {
									image_index = 0;	
								}			
								atkCounter++;
								if atkCounter >= atkCounterMax{				
									atkCounter = 0;
									isAtk = false;
								}			
							}
						#endregion
					}				
					break;		   
			}
		}
	#endregion
	
}
