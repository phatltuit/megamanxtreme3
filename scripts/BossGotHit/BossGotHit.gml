// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BossGotHit(){
	var playerBullet = instance_place(x , y, objPlayerProjectile);
	if playerBullet >= 0  and !isHit and !isGuard{
		SetHitEffLocation(playerBullet);
		hitCounterMax = 30;	
		if playerBullet.damage >= hp {	
			hp = 0;
			if instance_exists(objPlayer) {
				if objPlayer.isSkillAtk {
					objPlayer.isSkillAtk = false;
					objPlayer.skillCounter = 0;
					objPlayer.grav = 0.25;
					objPlayer.isDash = false;
				}				
			}
			EnemyEffect(spr_enemy_small_explosion, playerBullet.x > x?1:-1,bbox_hit,playerBullet.y);	
			state = BossState.Defeated;
			canHit = false;
			audio_stop_sound(global.music);
		}else{						
			hp -= playerBullet.damage;			
			isHit = true;	
			isPushBack = CanPushBack(playerBullet.sprite_index)*canPushBack;
			EnemyEffect(spr_enemy_small_hit, playerBullet.x > x?1:-1, bbox_hit,playerBullet.y);		
		}		
	}
	
	var playerBulletExpl = instance_place(x , y, objPlayerProjectileExplosion);
	if playerBulletExpl >= 0  and !isHit and !isGuard{
		SetHitEffLocation(playerBulletExpl);		
		hitCounterMax = 30;		
		if playerBulletExpl.damage >= hp {	
			hp = 0;
			EnemyEffect(spr_enemy_small_explosion, playerBulletExpl.x > x?1:-1,bbox_hit,playerBulletExpl.y);	
			//instance_destroy(playerBulletExpl);
			state = BossState.Defeated;
			canHit = false;
			audio_stop_sound(global.music);
		}else{						
			hp -= playerBulletExpl.damage;			
			isHit = true;				
			EnemyEffect(spr_enemy_small_hit,  playerBulletExpl.x > x?1:-1,bbox_hit,playerBulletExpl.y);
		}		
	}
	
	var playerSaber = instance_place(x , y, objSaber);
	if playerSaber >= 0 and !isHit and !isGuard{
		SetHitEffLocation(playerSaber);
		switch (playerSaber.sprite_index) {
		    case spr_z_crouch_slash:
			case spr_z_climb_slash_mask:
			case spr_z_jump_slash_mask:
			case spr_z_jump_slash_mask_0:
			case spr_z_wall_slide_slash_mask:
			case spr_z_wall_slide_slash_mask_0:
			case spr_z_stand_slash_mask_3:
			case spr_z_roll_slash_mask:			
			//Will be some skill here
		        hitCounterMax = 45;
		        break;
				
			case spr_z_skill_thunder_uppercut_mask:
				hitCounterMax = 30;
				break;
				
			case spr_z_stand_slash_mask_2:
			case spr_z_stand_slash_mask_6:
				if instance_exists(objZ){
					if objZ.slashStateMax == 2 {
						hitCounterMax = 70;	
					}else{
						hitCounterMax = 5;
					}						
				}
				break;
				
			case spr_z_stand_slash_mask_0:
			case spr_z_stand_slash_mask_1:
			case spr_z_stand_slash_mask_4:
			case spr_z_stand_slash_mask_5:
				hitCounterMax = 5;
		        break;
			
		    default:
		        // code here
		        break;
		}
		
		if playerSaber.damage >= hp{	
			hp = 0;
			EnemyEffect(spr_enemy_small_explosion, playerSaber.x > x?1:-1,bbox_hit,playerSaber.y-16);	
			audio_stop_sound(global.music);
			state = BossState.Defeated;
			canHit = false;			
		}else{						
			hp -= playerSaber.damage;			
			isHit = true;
			isPushBack = CanPushBack(playerSaber.sprite_index)*canPushBack;
			EnemyEffect(spr_enemy_small_hit, playerSaber.x > x?1:-1,bbox_hit, playerSaber.y-16);	
		}
	}
	
	if isHit or isGuard{	
		//if playerBullet >= 0 {		
		//	SetHitEffLocation(playerBullet);
		//	if IsDestroyable(playerBullet.sprite_index){
		//		EnemyEffect(spr_enemy_resist, bbox_hit, playerBullet.y);
		//		instance_destroy(playerBullet);
		//	}else{
		//		if isGuard or get_sprite_of(playerBullet) == spr_ma_buster_lv3_explosion{
		//			EnemyEffect(spr_enemy_resist, bbox_hit, playerBullet.y);	
		//		}else{
		//			EnemyEffect(spr_enemy_small_hit, bbox_hit, playerBullet.y);
		//		}
		//	}	
		//}
		if playerBulletExpl >= 0 {		
			SetHitEffLocation(playerBulletExpl);
			if hitCounter mod 5 == 0 {
				EnemyEffect(spr_enemy_resist, playerBulletExpl.x > x?1:-1,bbox_hit, playerBulletExpl.y);				
			}
		}		
		//if playerSaber >=0 {	
		//	SetHitEffLocation(playerSaber);
		//	if instance_exists(objZ){
		//		if objZ.slashStateMax == 2 and get_sprite_of(playerSaber) == spr_z_stand_slash_mask_2 {
		//			EnemyEffect(spr_enemy_resist, bbox_hit, playerSaber.y-16);				
		//		}else if objZ.slashStateMax == 3 and get_sprite_of(playerSaber) == spr_z_stand_slash_mask_3 {
		//			EnemyEffect(spr_enemy_resist, bbox_hit, playerSaber.y-16);		
		//		}else if isGuard {
		//			EnemyEffect(spr_enemy_resist, bbox_hit, playerSaber.y-16);			
		//		}else{
		//			EnemyEffect(spr_enemy_small_hit, bbox_hit, playerSaber.y-16);		
		//		}
		//	}else{
		//		EnemyEffect(spr_enemy_small_hit, bbox_hit, playerSaber.y-16);		
		//	}					
		//}
		
		if isHit {
			hitCounter++;
			switch (hitCounter/2 mod 3) {
			    case 0:
			        image_alpha = 0;
			        break;
				
				case 1:
			        image_alpha = 0;
			        break;
				
				case 2:
			        image_alpha = 1;
			        break;
			    default:
			        // code here
			        break;
			}
			if hitCounter >= hitCounterMax {
				hitCounter = 0;
				isHit = false;
				isPushBack = false;
				image_alpha = 1;
				prev_hp = hp;
			}
		}
	}
}