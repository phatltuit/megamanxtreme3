// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BossDefeated(){
	set_sprite(sprHit);
	if animation_end(){
		frame_stop(image_number - 1);	
	}
	objPlayer.isBlocked = true;	
	instance_destroy(objBossAtk);		
	if explodeCounter < explodeCounterMax {
		explodeCounter++;	
		rd_expl = irandom(1) == 1? spr_boss_explosion : spr_enemy_small_explosion;
		rd_expl_xscale =  irandom(1)?1:-1;
		if explodeCounter >= 120  and explodeCounter < 300 {
			if explodeCounter mod 30 == 0 {					
				EnemyEffect(rd_expl, rd_expl_xscale, irandom_range(bbox_left, bbox_right),irandom_range(bbox_top, bbox_bottom));
			}
		}else if explodeCounter >= 300 and explodeCounter < 500{
			if explodeCounter mod 20 == 0 {
				EnemyEffect(rd_expl, rd_expl_xscale, irandom_range(bbox_left, bbox_right),irandom_range(bbox_top, bbox_bottom));
			}	
		}else if explodeCounter >= 500 and explodeCounter < 600{
			if explodeCounter mod 10 == 0 {
				EnemyEffect(rd_expl, rd_expl_xscale, irandom_range(bbox_left, bbox_right),irandom_range(bbox_top, bbox_bottom));
			}		
		}else if explodeCounter >= 600 and explodeCounter < 800 {
			if explodeCounter == 600 {
				white_scr = instance_create_depth(get_view_x(), get_view_y(), depth-101, objEnemyEffect);
				white_scr.image_alpha = 0;
				white_scr.sprite_index = spr_dead_scr;
				white_scr.depth = depth+1;
			}
			white_scr.image_alpha += 0.005;
			if explodeCounter mod 10 == 0 {
				EnemyEffect(rd_expl,  rd_expl_xscale,irandom_range(bbox_left, bbox_right),irandom_range(bbox_top, bbox_bottom));
			}		
			if white_scr.image_alpha > 0.8 {
				image_alpha-=0.08;	
			}
		}else if explodeCounter >= 800{
			white_scr.image_alpha -= 0.01;
			if explodeCounter == 800 {
				play_sfx(-1, sfx_boss_explosion);
				visible = false;
			}	
			if explodeCounter == 1400 {
				white_scr.image_alpha = 0;
				objPlayer.isMissionComplete = true;
				instance_destroy();
			}
		}
	}
}