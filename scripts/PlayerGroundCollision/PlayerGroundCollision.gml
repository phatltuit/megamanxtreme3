// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerGroundCollision(){
	for (var i = 0; i < bbox_right-bbox_left; ++i) {
	     if tilemap_get_at_pixel(tilemap, bbox_left + i, bbox_bottom + 1) != 0{
			 return true;
		 }
	}
	return false;
}