// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function boss_draw(){	
	
	if isHit and hitCounter<=8{
		shader_set(sh_hit_flash);
		shader_set_uniform_f(shader_get_uniform(sh_hit_flash, "flashColor"), 1.0);	
		draw_sprite_ext(sprite_index, image_index,x,y,image_xscale, image_yscale, image_angle, image_blend, image_alpha);		
		shader_reset()
	}	
	
	if explodeCounter >= 600{
		shader_set(sh_hit_flash);		
		shader_set_uniform_f(shader_get_uniform(sh_hit_flash, "flashColor"), 0.0);	
		draw_sprite_ext(sprite_index, image_index,x,y,image_xscale, image_yscale, image_angle, image_blend, image_alpha);
		shader_reset();
	}
			
	if state != BossState.Deactive {
		boss_draw_hud();
	}	
	
}