// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BossCollision(){
	
	if hsp > 0{
		bbox_x = bbox_right;
	}else if hsp < 0{
		bbox_x = bbox_left;	
	}
	 
	////Check tile on the right or left
	if !place_meeting(x+hsp, y, objSlope)
	and (tilemap_get_at_pixel(tilemap, bbox_x + hsp, bbox_bottom) != 0
	or tilemap_get_at_pixel(tilemap, bbox_x + hsp, bbox_top) != 0) {			
		for(i = bbox_top;i <= bbox_bottom;i++){
			if tilemap_get_at_pixel(tilemap, bbox_x + hsp , i) != 0 {
				var pixel = 0;
				while tilemap_get_at_pixel(tilemap, bbox_x+pixel, i) == 0 {
					pixel+=sign(hsp);	
				}
				x += pixel-sign(hsp);								
				break;	
			}	
		}		
		hsp = 0;
	}
	
	//Horizontal Collision
	if place_meeting(x+hsp,y,objSlope)
	{
	    yplus = 0;
	    while (place_meeting(x+hsp,y-yplus,objSlope) and yplus <= abs(1*hsp)) yplus += 1;
	    if place_meeting(x+hsp,y-yplus,objSlope)
	    {
	        while (!place_meeting(x+sign(hsp),y,objSlope)) x+=sign(hsp);
	        hsp = 0;
	    }
	    else
	    {
	        y -= yplus
	    }
	}
	
	x+=hsp;
		
	 
	if vsp >= 0{
		bbox_y = bbox_bottom;
	}else{
		bbox_y = bbox_top;	
	}
	
	//Down Slope
	if !place_meeting(x,y,objSlope) and vsp >= 0 
	and (place_meeting(x,y+2+abs(hsp),objSlope) or
	tilemap_get_at_pixel(tilemap, x, bbox_bottom+2+abs(hsp)) != 0)
	{
		if place_meeting(x,y+2+abs(hsp),objSlope){
			while !place_meeting(x,y+1,objSlope) {
				y += 1;
			}
		}else if tilemap_get_at_pixel(tilemap, x, bbox_bottom+2+abs(hsp)) != 0{
			while tilemap_get_at_pixel(tilemap, x, bbox_bottom+1) == 0 {
				y += 1;
			}	
		}
	}
	
	//Check tile on the top or bottom
	if tilemap_get_at_pixel(tilemap, bbox_right, bbox_y + vsp) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_left, bbox_y + vsp) != 0 {	
		if vsp >= 0 {
			for(i = bbox_left;i <= bbox_right;i++){
				if tilemap_get_at_pixel(tilemap, i, bbox_bottom + vsp+1) != 0 {
					var pixel = 0;
					while tilemap_get_at_pixel(tilemap, i, bbox_bottom+pixel) == 0 {
						pixel++;	
					}
					y += pixel-1;								
					break;	
				}	
			}		
			on_ground = true;
			if object_get_parent(self) == objBoss play_sfx(-1, sfx_boss_landing);
		}else{
			var i;
			for(i = bbox_left;i <= bbox_right;i++){
				if tilemap_get_at_pixel(tilemap, i, bbox_top + vsp) != 0 {
					var pixel = 0;
					while tilemap_get_at_pixel(tilemap, i, bbox_top-pixel) == 0 {
						pixel++;	
					}
					y -= pixel-1;
					break;	
				}
			}
		}
		vsp = 0;
	}
	
	//Vertical Collisionsd
	var slope = instance_place(x,y+vsp,objSlope);
	if slope >= 0{
		if vsp >= 0 {
			y = slope.y;
			while place_meeting(x,y,objSlope) y--;
			vsp = 0;
			on_ground = true;	
			if object_get_parent(self) == objBoss play_sfx(-1, sfx_boss_landing);
		}else{
			y = slope.y;
			while place_meeting(x,y,objSlope) y++;
			vsp = 0;
		}
	}
	
	y+=vsp;
	
	if tilemap_get_at_pixel(tilemap, bbox_right, bbox_bottom + 1) == 0 
	and tilemap_get_at_pixel(tilemap, bbox_left, bbox_bottom + 1) == 0 
	and tilemap_get_at_pixel(tilemap, x, bbox_bottom + 1) == 0
	and !place_meeting(x, y + 1, objSlope){
		on_ground = false	
	}
	
	if on_ground {
		vsp = 0;
	}else{
		vsp += grav;	
		if vsp >= 5 {
			vsp = 5;	
		}
	}	
}

function BossAtkCollision() {
	if hsp > 0{
		bbox_x = bbox_right;
	}else if hsp < 0{
		bbox_x = bbox_left;	
	}
	 
	////Check tile on the right or left
	if !place_meeting(x+hsp, y, objSlope)
	and (tilemap_get_at_pixel(tilemap, bbox_x + hsp, bbox_bottom) != 0
	or tilemap_get_at_pixel(tilemap, bbox_x + hsp, bbox_top) != 0) {			
		x-=sign(hsp)
		hsp = 0;
	}
	
	//Horizontal Collision
	if place_meeting(x+hsp,y,objSlope)
	{
	    yplus = 0;
	    while (place_meeting(x+hsp,y-yplus,objSlope) and yplus <= abs(1*hsp)) yplus += 1;
	    if place_meeting(x+hsp,y-yplus,objSlope)
	    {
	        while (!place_meeting(x+sign(hsp),y,objSlope)) x+=sign(hsp);
	        hsp = 0;
	    }
	    else
	    {
	        y -= yplus
	    }
	}
	
	x+=hsp;
		
	 
	if vsp >= 0{
		bbox_y = bbox_bottom;
	}else{
		bbox_y = bbox_top;	
	}
	
	//Down Slope
	if !place_meeting(x,y,objSlope) and vsp >= 0 
	and (place_meeting(x,y+2+abs(hsp),objSlope) or
	tilemap_get_at_pixel(tilemap, x, bbox_bottom+2+abs(hsp)) != 0)
	{
		if place_meeting(x,y+2+abs(hsp),objSlope){
			while !place_meeting(x,y+1,objSlope) {
				y += 1;
			}
		}else if tilemap_get_at_pixel(tilemap, x, bbox_bottom+2+abs(hsp)) != 0{
			while tilemap_get_at_pixel(tilemap, x, bbox_bottom+1) == 0 {
				y += 1;
			}	
		}
	}
	
	//Check tile on the top or bottom
	if tilemap_get_at_pixel(tilemap, bbox_right, bbox_y + vsp) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_left, bbox_y + vsp) != 0 {		
		
		if vsp >= 0 {
			for(i = bbox_left;i <= bbox_right;i++){
				if tilemap_get_at_pixel(tilemap, i, bbox_bottom + vsp+1) != 0 {
					var pixel = 0;
					while tilemap_get_at_pixel(tilemap, i, bbox_bottom+pixel) == 0 {
						pixel++;	
					}
					y += pixel-1;								
					break;	
				}	
			}		
			on_ground = true;
		}else{
			var i;
			for(i = bbox_left;i <= bbox_right;i++){
				if tilemap_get_at_pixel(tilemap, i, bbox_top + vsp) != 0 {
					var pixel = 0;
					while tilemap_get_at_pixel(tilemap, i, bbox_top-pixel) == 0 {
						pixel++;	
					}
					y -= pixel-1;
					break;	
				}
			}
		}
		vsp = 0;
	}
	
	//Vertical Collisionsd
	var slope = instance_place(x,y+vsp,objSlope);
	if slope >= 0{
		if vsp >= 0 {
			y = slope.y;
			while place_meeting(x,y,objSlope) y--;
			vsp = 0;
			on_ground = true;	
			if object_get_parent(self) == objBoss play_sfx(-1, sfx_boss_landing);
		}else{
			y = slope.y;
			while place_meeting(x,y,objSlope) y++;
			vsp = 0;
		}
	}
	
	y+=vsp;
	
	if tilemap_get_at_pixel(tilemap, bbox_right, bbox_bottom + 1) == 0 
	and tilemap_get_at_pixel(tilemap, bbox_left, bbox_bottom + 1) == 0 
	and tilemap_get_at_pixel(tilemap, x, bbox_bottom + 1) == 0
	and !place_meeting(x, y + 1, objSlope){
		on_ground = false	
	}
	
	if on_ground {
		vsp = 0;
	}else{
		vsp += grav;	
		if vsp >= 5 {
			vsp = 5;	
		}
	}		
}