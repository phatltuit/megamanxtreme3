// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerDash(){
	if !isHyperJump and canDash and !isWallSlide{ //if player is in hyper jump, then he can't air dash
		//When holding the dash button, set dash state on
		if key_dash == 1{
			isDash = true;	
			play_sfx(4, sfx_dash);			
		}
		
		//Moving player when dash state is on.
		if (key_dash_hold == 1 and isDash and !isClimb and !isPushBack and dashCounter < dashCounterMax) or (isDash and PlayerHeadCollision()){	
			if sprite_is(sprDash) or sprite_is(sprDashAtk) {
				if image_index > 6{
					image_index = 1;	
				}			
			}		
			
			hsp = image_xscale*dashSpd;
		
			if isAtk and is_Zero() {
				isAtk = false;
				set_sprite_of(sword, noone);			
			}
			dashCounter++;			
			if dashCounter == dashCounterMax {
				image_index = 6;	
			}	
		}
		
		
		//End dash if release dash button without coliding any solid on the top
		if ((!on_ground and key_dash_release) or (on_ground and !key_dash_hold)) and !PlayerHeadCollision(){
			canPlaySFX[4] = true;	
			if (sprite_is(sprDash) or sprite_is(sprDashAtk)) and animation_end(){
				dashCounter = 0;
				isDash = false;	
				canDash = on_ground;				
			}
			if dashCounter >= dashCounterMax and !isDash{
				dashCounter = 0;	
				canDash = on_ground;				
			}else{
				isDash = false;
				dashCounter = 0;
				canDash = on_ground;				
			}
		}
		
		if isDash and ((image_xscale == 1 and !canGoRight) or (image_xscale = -1 and !canGoLeft)) and !place_meeting(x,y+1,objSlope){
			isDash = false;
			dashCounter = 0;
			canDash = on_ground;			
		}
		
		//End dash with animation
		if dashCounter >=dashCounterMax and isDash and !PlayerHeadCollision(){
			canPlaySFX[4] = true;
			hsp += image_xscale*dashSpd/3;
			if (sprite_is(sprDash) or sprite_is(sprDashAtk)) and animation_end() {				
				isDash = false;					
				if !key_dash_hold {
					dashCounter = 0;
					canDash = on_ground;
				}
			}
		}
	}	
}