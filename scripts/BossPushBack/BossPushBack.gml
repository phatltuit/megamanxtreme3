// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BossPushBack(){
	if isPushBack {	
		set_sprite(sprHit);	
		if on_ground {
			hsp = -pushBackSpd*image_xscale;	
			BossCollision();
		}
	
		if 	pushBackCounter < pushBackCounterMax {
			pushBackCounter++;
		}else{
			pushBackCounter = 0;
			isPushBack = false;	
			if on_ground {
				hsp = 0;
			}
		}
	}
}