// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function BossCreateEvent(){
	image_xscale = -1;
	damage = 3;
	hp = 0;
	prev_hp = 64;
	maxHp = 64;
	grav = 0.25;
	pushBackSpd = 0.1;
	prev_image_index = 0;

	bbox_x = bbox_left;
	bbox_y = bbox_bottom;

	enum BossState{
		Active,
		Deactive,
		Begin,
		Defeated
	}

	//state = BossState.Deactive;
	state = BossState.Deactive;

	isHit = false;
	canHit = true;
	isGuard = false;
	isDead = false;
	on_ground = true;
	isPushBack = false;	
	isAttack = false;
	ultimate = false;
	canPushBack = false;
	

	hsp = 0;
	vsp = 0;
	spd = 0;
	phase = 0;
	phaseStart = false;

	beginCounter = 0;
	beginCounterMax = 2;
	hitCounter = 0;
	hitCounterMax = 5;
	jumpCounter = 0;
	jumpCounterHalf = 0;
	phaseCounter = 0;
	pushBackCounter = 0;
	pushBackCounterMax = 10;
	explodeCounter = 0;
	explodeCounterMax = 1401;
	
	//Atk counter
	rushCounter = 0;
	

	//Tile map, using for primary collision, must be create before the player layer.
	tilemap = layer_tilemap_get_id(layer_get_id("Collision"));	
	depth = get_highest_depth_layer()-99;
	particle_sys = part_system_create();
	part_system_depth(particle_sys, depth + 1);	
}