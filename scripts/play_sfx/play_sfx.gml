 // Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function play_sfx(index, sound_id){
	if index >= 0{
		if canPlaySFX[index] {
			audio_stop_sound(sound_id);
			audio_sound_gain(sound_id, 1,0);
			audio_play_sound(sound_id, 0, false);	
			canPlaySFX[index] = false;
		}
	}else{
		audio_sound_gain(sound_id, 1,0);
		audio_play_sound(sound_id, 0, false);	
	}
}