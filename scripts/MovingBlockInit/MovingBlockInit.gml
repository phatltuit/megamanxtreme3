// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function MovingBlockInit(hspd, vspd, h_direction, v_direction,h_moving_time, v_moving_time,h_delay_time, v_delay_time){
	h_delay = h_delay_time;
	v_delay = v_delay_time;
	v_time = v_moving_time;
	h_time = h_moving_time;
	vertical_speed = vspd;
	horizone_speed = hspd;
	xDir = h_direction;
	yDir = v_direction;
}