// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerDead(){
	if deadCounter < deadCounterMax {
		audio_stop_sound(sfx_charge_lv2);
		set_sprite(sprDead);
		deadCounter++;		
		if deadCounter >= 60 {
			if deadCounter == 60{
				play_sfx(-1, sfx_dead);
				white_scr = instance_create_depth(get_view_x(), get_view_y(), depth-101, objPlayerEffect);
				white_scr.image_alpha = 0;
				white_scr.sprite_index = spr_dead_scr;
			}
				
			white_scr.image_alpha+=0.005;
		}
		if deadCounter mod 60 == 0 {
			soul = is_Zero()?spr_z_soul : spr_x_soul
			for (var i = 1; i <= 16; ++i){
				var expl = instance_create_depth(x,y,depth, objPlayerExplosion);
				set_sprite_of(expl, soul);
				expl.speed = 6;
				expl.direction = i*22.5;
			}
			for (var i = 1; i <= 16; ++i){
				var expl = instance_create_depth(x,y,depth, objPlayerExplosion);
				set_sprite_of(expl, soul);
				expl.speed = 3;
				expl.direction = i*22.5;
			}
		}
	}else{
		game_restart();	
	}
}