// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerCheckWallCollide(){
	return tilemap_get_at_pixel(tilemap, bbox_horizone + hsp, bbox_top) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_horizone + hsp, bbox_bottom) != 0
	or tilemap_get_at_pixel(tilemap, bbox_horizone + hsp, y) != 0
	
	//or PlayerSideCollision();
}