// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerSwitchSection(){
	if isFreeze and !isBlocked{
		//Sprite when transition
		switch (sprite_index) {	    
			case sprClimb:
			case sprRun:
				break;
			
			default:
				image_speed = 0;
				break;
		}
		
		
		//Check type of transition then do the transition	
		switch (transition_type) {
			case Transition.Right:
				hsp = 2*(sectionRight - bbox_left + 36)/transitionHorizoneTimeLimit;
				vsp = 0;			    
				image_alpha = 1;
				transitionTime++;
				cameraX += transitionSpd;
				camera_set_view_pos(view_camera[0], cameraX, cameraY);
				if transitionTime >= transitionHorizoneTimeLimit{
					transitionTime = 0;
					PlayerCameraInit();
					ds_list_clear(view_collided);		
					isSkillAtk = false;
					grav = 0.25;
					image_speed = 1;
					if isDash {
						isDash = false;
						dashCounter = 0;
						canPlaySFX[4] = true;
					}
					var currentView = instance_place(x,y, objViewBoudary);
					if currentView.isBossView{
						audio_sound_gain(global.music, 0, 2000);						
						ex_hsp = 0;
						transition_type = Transition.BossAppear;
						hsp = 0;
						isBlocked = true;
					}
					isFreeze = false;
				}					
			    break;
				
			case Transition.Left:
				hsp = -2*abs(bbox_right - sectionLeft+36)/transitionHorizoneTimeLimit;
				vsp = 0;		
				image_alpha = 1;
				transitionTime++;
				cameraX -= transitionSpd;
				camera_set_view_pos(view_camera[0], cameraX, cameraY);
				if transitionTime >= transitionHorizoneTimeLimit{
					transitionTime = 0;
					PlayerCameraInit();
					ds_list_clear(view_collided);	
					isSkillAtk = false;
					grav = 0.25;
					image_speed = 1;
					if isDash {
						isDash = false;
						dashCounter = 0;
						canPlaySFX[4] = true;						
					}
					var currentView = instance_place(x,y, objViewBoudary);
					if currentView.isBossView{
						audio_sound_gain(global.music, 0, 2000);						
						ex_hsp = 0;
						transition_type = Transition.BossAppear;
						hsp = 0;
						isBlocked = true;
					}
					isFreeze = false;				
				}					
			    break;
				
			case Transition.Down:				
				vsp = 2*(sectionBottom - bbox_top + 32)/transitionVerticalTimeLimit; 
				hsp = 0;			  
				image_alpha = 1;
				transitionTime++;
				cameraY += transitionSpd;
				camera_set_view_pos(view_camera[0], cameraX, cameraY);
				if transitionTime >= transitionVerticalTimeLimit{
					transitionTime = 0;
					PlayerCameraInit();
					ds_list_clear(view_collided);
					image_speed = 1;
					isSkillAtk = false;
					grav = 0.25;
					var currentView = instance_place(x,y, objViewBoudary);
					if currentView.isBossView{
						audio_sound_gain(global.music, 0, 2000);						
						ex_hsp = 0;
						transition_type = Transition.BossAppear;
						hsp = 0;
						isBlocked = true;
					}
					isFreeze = false;
				}					
			    break;
				
			case Transition.Up:
				vsp = -2*(bbox_bottom-sectionTop+36)/transitionVerticalTimeLimit; 
				hsp = 0;			
				image_alpha = 1;
				transitionTime++;
				cameraY -= transitionSpd;
				camera_set_view_pos(view_camera[0], cameraX, cameraY);
				if transitionTime >= transitionVerticalTimeLimit{
					transitionTime = 0;
					PlayerCameraInit();
					ds_list_clear(view_collided);
					image_speed = 1;
					isSkillAtk = false;
					grav = 0.25;
					var currentView = instance_place(x,y, objViewBoudary);
					if currentView.isBossView{
						audio_sound_gain(global.music, 0, 2000);						
						ex_hsp = 0;
						transition_type = Transition.BossAppear;
						hsp = 0;
						isBlocked = true;
					}
					isFreeze = false;
				}					
			    break;
			
			default:
				break;
		}
	}
}