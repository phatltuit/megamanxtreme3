// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function face_to_player(){
	if instance_exists(objPlayer){
		if objPlayer.x > x {
			image_xscale = 1;	
		}else{
			image_xscale = -1;	
		}
	}
}