// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerInput(){
	if !isSkillAtk and !isBlocked{
		key_up = keyboard_check(ord("W"))*(!isGetUp);
		key_down = keyboard_check(ord("S"))*(!isGetUp);
		key_left = keyboard_check(ord("A"));
		key_right = keyboard_check(ord("D"));
		key_jump = keyboard_check_pressed(ord("K"));
		key_dash = keyboard_check_pressed(ord("L"));
		key_dash_hold = keyboard_check(ord("L"));	
		key_dash_release = keyboard_check_released(ord("L"));
		key_next_wp = keyboard_check_pressed(ord("I"));
		key_prev_wp = keyboard_check_pressed(ord("U"));
		key_giga_attack = keyboard_check_pressed(ord("O"));
		key_skill_attack = keyboard_check_pressed(ord("N"));
	
		if !is_Zero(){
			global.weaponIndex += (key_next_wp - key_prev_wp);
			PlayerWeaponSwitch();
		}
	
		switch (character) {
		    case Character.Zero:
		        key_attack_release = keyboard_check_pressed(ord("J"));
		        break;
			
			case Character.X:
				key_attack_hold = keyboard_check(ord("J"));
				key_attack_release = keyboard_check_released(ord("J"));
				break;

		}
	}
	
	if isBlocked {
		key_up = 0;
		key_down = 0;
		key_left = 0;
		key_right = 0;
		key_jump = 0;
		key_dash = 0;
		key_dash_hold = 0;
		key_dash_release = 0;
		key_next_wp = 0;
		key_prev_wp = 0;
		key_giga_attack = 0;
		key_skill_attack = 0;
		
		if transition_type == Transition.BossAppear{	
			if on_ground and objBoss.state == BossState.Deactive {
					
				if waitCounter < waitCounterMax {
					if waitCounter == 120 {
						audio_play_sound(sfx_warning, 0, false);	
					}
					if waitCounter == 420 {
						audio_sound_gain(sfx_warning, 0, 1000);		
					}
					waitCounter++;
				}else{						
					objBoss.state = BossState.Begin;							
					image_speed = 1;
				}
			}
		}
	}
}