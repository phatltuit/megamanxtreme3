// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerJump(){
	//Check if player is near the wall
	isNearWall = (PlayerNearWall() and !on_ground and !isWallJump);	
	
	if isWallSlide {
		if instance_number(objPlayerEffect) < 15{
			wallSlideCounter++;			
			if wallSlideCounter mod 5 == 0 and wallSlideCounter >= 10 {
				wall_slide_eff = instance_create_depth(x+image_xscale*12, bbox_bottom, depth - 1, objPlayerEffect);
				set_sprite_of(wall_slide_eff, spr_wall_slide_eff);
				wall_slide_eff.alarm[0] = 40;	
			}
		}		
	}else{
		wallSlideCounter = 0;	
	}
	
	//Set the wall side direction
	if isNearWall {
		wall_side = PlayerWallSide();	
		if key_jump {
			isWallJump = true;		
			isWallSlide = false;
			isNearWall = false;
			wallJumpCounter = 0;
			image_index = 0;
			wall_jump_eff = instance_create_depth(x+image_xscale*12, bbox_bottom, depth, objPlayerEffect);
			set_sprite_of(wall_jump_eff, spr_wall_jump_eff);
			wall_jump_eff.alarm[0] = 12;
			play_sfx(-1,sfx_jump);
		}				
	}
	
	
	//Wall Jump
	if isWallJump {			
		show_debug_message("Wall jump frame["+string(wallJumpCounter)+"]");
		wallJumpCounter++;	
		if sprite_is(sprWallJump) and animation_end() and wallJumpCounter < wallKickMax{
			frame_stop(image_number - 1)			
		}
		if wallJumpCounter >= wallKickMax{
			hsp = -(wall_side*(isHyperJump?(dashSpd*3/2):runSpd));
			doWallJump = true;
			if wallJumpCounter == wallKickMax and keyboard_check(ord("K")){				
				vsp = -jumpSpd*(!isClimb);					
				//Double Jump
				if canDouleJump and !on_ground and !isWallJump {
					canDouleJump = false;
					canDash = isWallJump;
				}
				
				//Jump when holding dash button
				if isDash or key_dash_hold { 			
					isHyperJump = true;
					isDash = false;
					dashCounter = 0;	
					//canDouleJump = false;
				}		
				
				canFly = true;
				minJump = true;	
				isClimb = false;
				on_ground = false;		
				canPlaySFX[2] = true;
				canPlaySFX[4] = true;
				canPlaySFX[5] = true;
			}else if wallJumpCounter == wallKickMax and !keyboard_check(ord("K")) {
				vsp = -jumpSpd*(!isClimb)/3;
				if canDouleJump and !on_ground and !isWallJump {
					canDouleJump = false;
					canDash = isWallJump;
				}
				
				//Jump when holding dash button
				if isDash or key_dash_hold { 			
					isHyperJump = true;
					isDash = false;
					dashCounter = 0;	
					//canDouleJump = false;
				}		
				
				canFly = true;
				//minJump = true;	
				isClimb = false;
				on_ground = false;	
				canPlaySFX[2] = true;
				canPlaySFX[4] = true;
				canPlaySFX[5] = true;				
			}else{
				canPlaySFX[6] = true;				
			}
		}else{
			image_xscale = wall_side;
			vsp = 0;
		}
		if wallJumpCounter >= wallJumpCounterMax{
			isWallJump = false;
		}
	}
	
	
	if key_jump and (on_ground or isClimb or ((is_Zero() or armor_type == ArmorType.Beta) and canDouleJump and !isHyperJump and !isWallSlide)) and !PlayerHeadCollision(){	
		vsp = -jumpSpd*(!isClimb);	
	

		//Jump when holding dash button
		if isDash or key_dash_hold { 			
			isHyperJump = true;
			isDash = false;
			dashCounter = 0;	
			//canDouleJump = false;
		}		
		
		//Double Jump
		if canDouleJump and !on_ground and !isWallJump {
			canDouleJump = false;
			canDash = isWallJump;			
		}

		
		//Jump when climbing
		if isClimb {
			isClimb = false;
			canDash = true;
			canDouleJump = true;
			image_speed = 1;
		}else{
			audio_play_sound(sfx_jump, 0, false);	
		}
		
		minJump = true;	
		isClimb = false;
		on_ground = false;	
		canPlaySFX[2] = true;
		canPlaySFX[4] = true;
		canPlaySFX[5] = true;
	}
	
	
	//Horizone Speed when in Hyper Jump	
	if isHyperJump {
		runSpd = hyperSpd;	
	}else{
		runSpd = isOverride ? 2.5 : runSpeed;
	}
	
	
	//Falling if no ground
	if tilemap_get_at_pixel(tilemap, bbox_right, bbox_vertical + vsp + 1) == 0 
	and tilemap_get_at_pixel(tilemap, bbox_left, bbox_vertical + vsp + 1) == 0
	and !place_meeting(x, y + 1, objTopLadder)
	and !place_meeting(x, y + 1, objSlope){
		on_ground = false;
		canPlaySFX[5] = true;
	}

	//Stop jumping up when hit the ceil or release the Jump button or meet the limit jump height
	if (!keyboard_check_direct(ord("K")) or PlayerHeadCollision()) and minJump and vsp < 0 and !on_ground{		
		if isWallJump {
			isWallJump = false;
		}		
		vsp = 0;
		minJump = false;
	}
	
	//Stop falling when perform air dashing
	if !on_ground and isDash and !isHyperJump {	
		vsp = 0;
		minJump = false;
		canDouleJump = false;
	}
	
	//Reset variables when on the ground
	if on_ground {		
		vsp = 0;
		isHyperJump = false;	
		canDouleJump = true;
		canDash = true;		
	}else{	// if player is not climbing ladder and dashing on the air, then player will be pull down by the gravity force
		if !isClimb and !isDash and ((!is_Zero() and !isSkillAtk) or is_Zero()) {
			vsp += grav;
			if vsp >= 5 {
				vsp = 5;
			}
		}
	}
}