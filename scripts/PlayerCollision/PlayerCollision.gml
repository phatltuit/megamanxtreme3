// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerCollision(){	
//Vertical Collision
	//Tile collision
	if vsp >= 0{
		bbox_vertical = bbox_bottom;
	}else{
		bbox_vertical = bbox_top;	
	}
	 
	//Check tile on the top or bottom		
	if (tilemap_get_at_pixel(tilemap, bbox_left, bbox_vertical + vsp + 1) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_right, bbox_vertical + vsp + 1) != 0 
	or tilemap_get_at_pixel(tilemap, x, bbox_vertical + vsp + 1) != 0) {
		if vsp > 0{						
			play_sfx(2, sfx_walk);	
			var i;
			for(i = bbox_left;i <= bbox_right;i++){
				if tilemap_get_at_pixel(tilemap, i, bbox_bottom + vsp + 1) != 0 {
					var pixel = 0;
					y = floor(y);
					while tilemap_get_at_pixel(tilemap, i, bbox_bottom+pixel) == 0 {
						pixel++;	
					}
					y += pixel-1;								
					break;	
				}	
			}
			isWallSlide = false;
			if isClimb {
				isClimb = false;	
			}			
			on_ground = true;				
			canPlaySFX[4] = true;
			isRollAtk = false;			
		}else if vsp < 0{
			var i;
			for(i = bbox_left;i <= bbox_right;i++){
				if tilemap_get_at_pixel(tilemap, i, bbox_vertical + vsp) != 0 {
					var pixel = 0;
					while tilemap_get_at_pixel(tilemap, i, bbox_top-pixel) == 0 {
						pixel++;	
					}
					y -= pixel-1;
					break;	
				}
			}
						
			if isSkillAtk {
				isSkillAtk = false;
				skillCounter = 0;
			}
			if isWallJump {
				isWallJump = false;
			}			
		}		
		vsp = 0;
	}
	
	
	
	//Top ladder collision
	ladder = instance_place(x, y + vsp, objTopLadder);
	if ladder >= 0 and vsp < 0 and y <= ladder.bbox_bottom and isClimb and !isGetUp{
		isGetUp = true;
		image_index = 0;
	}
	if isGetUp {
		vsp = -0.25;
		getUpCounter++;
		if getUpCounter >= getUpCounterMax*2 {
			//y = y - (y mod 16) + 15 - abs(bbox_bottom - y);ddddddddd
			y = ladder.y - 1 - (bbox_bottom - round(y));
			isWallSlide = false;			
			on_ground = true;
			isRollAtk = false;
			vsp = 0;
			canPlaySFX[4] = true;
			isGetUp = false;		
			isClimb = false;
			getUpCounter = 0;
		}
	}
	
	if ladder >= 0 and vsp >= 0 and ladder.y > bbox_bottom-3 and !isClimb{
		//y = y - (y mod 16) + 15 - abs(bbox_bottom - y);		
		y = ladder.y - 1 - (bbox_bottom - round(y));
		isWallSlide = false;
		play_sfx(5, sfx_landing);
		on_ground = true;
		isRollAtk = false;
		vsp = 0;
		canPlaySFX[4] = true;
		isGetUp = false;
	}
//End	
	
//Horizone Collision
	//Tile collision
	if hsp > 0{
		bbox_horizone = bbox_right;	
	}else if hsp < 0{
		bbox_horizone = bbox_left;	
	}else{
		if image_xscale	== -1{
			bbox_horizone = bbox_left;		
		}else{
			bbox_horizone = bbox_right;	
		}
	}
	
	if !place_meeting(x+hsp, y, objSlope) and PlayerCheckWallCollide(){
		if hsp < 0{		
			//if isDash and !on_ground {
			//	isDash = false;
			//	dashCounter = 0;				
			//}			
			canDash = true;
			//x = x - (x mod tile_size) - (bbox_left - x);	
			var i;
			for(i = bbox_top;i <= bbox_bottom;i++){
				if tilemap_get_at_pixel(tilemap, bbox_left + hsp, i) != 0 {
					x = ceil(x);
					var pixel = 0;
					while tilemap_get_at_pixel(tilemap,  bbox_left - pixel, i) == 0 {
						pixel++;	
					}
					x -= pixel-1;
					break;	
				}
			}
			
			if key_left == 1 and !on_ground  and vsp >= 0{				
				isWallSlide = true;
				isHyperJump = false;
				vsp = wallSpd;						
				canDouleJump = true;
				canDash = true;	
				dashCounter = 0;
				isRollAtk = false;
				play_sfx(2, sfx_landing);	
			}else{
				if !isWallJump {
					isWallSlide = false;					
					canPlaySFX[2] = true;
				}
			}		
			hsp = 0;			
		}else if hsp > 0{		
			//if isDash and !on_ground {
			//	isDash = false;
			//	dashCounter = 0;	
			//}		
			canDash = true;
			//x = x - (x mod tile_size) + 15 - (bbox_right - x);	
			var i;
			for(i = bbox_top;i <= bbox_bottom;i++){
				if tilemap_get_at_pixel(tilemap, bbox_right + hsp, i) != 0 {
					var pixel = 0;
					x = floor(x);
					while tilemap_get_at_pixel(tilemap,  bbox_right + pixel, i) == 0 {
						pixel++;	
					}
					x += pixel-1;
					break;	
				}
			}			
			if key_right == 1 and !on_ground and vsp >= 0{				
				isWallSlide = true;		
				isHyperJump = false;
				vsp = wallSpd;						
				canDouleJump = true;	
				canDash = true;	
				dashCounter = 0;
				isRollAtk = false;
				play_sfx(2, sfx_landing);	
			}else{
				if !isWallJump {
					isWallSlide = false;
					canPlaySFX[2] = true;
				}
			}					
			hsp = 0;			
		}else{
			isWallSlide = false;			
			canPlaySFX[2] = true;
		}
	}else{
		isWallSlide = false;
		if !on_ground{
			canPlaySFX[2] = true;
		}
	}
}

function PlayerProjectileCollision() {
	if hsp > 0{
		bbox_x = bbox_right;
	}else if hsp < 0{
		bbox_x = bbox_left;	
	}
	 
	////Check tile on the right or left
	if !place_meeting(x+hsp, y, objSlope)
	and (tilemap_get_at_pixel(tilemap, bbox_x + hsp, bbox_bottom) != 0
	or tilemap_get_at_pixel(tilemap, bbox_x + hsp, bbox_top) != 0) {			
		for(i = bbox_top;i <= bbox_bottom;i++){
			if tilemap_get_at_pixel(tilemap, bbox_x + hsp , i) != 0 {
				var pixel = 0;
				while tilemap_get_at_pixel(tilemap, bbox_x+pixel, i) == 0 {
					pixel+=sign(hsp);	
				}
				x += pixel-sign(hsp);								
				break;	
			}	
		}		
		hsp = 0;
	}
	
	//Horizontal Collision
	if place_meeting(x+hsp,y,objSlope)
	{
	    yplus = 0;
	    while (place_meeting(x+hsp,y-yplus,objSlope) and yplus <= abs(1*hsp)) yplus += 1;
	    if place_meeting(x+hsp,y-yplus,objSlope)
	    {
	        while (!place_meeting(x+sign(hsp),y,objSlope)) x+=sign(hsp);
	        hsp = 0;
	    }
	    else
	    {
	        y -= yplus
	    }
	}
	
	x+=hsp;
		
	 
	if vsp >= 0{
		bbox_y = bbox_bottom;
	}else{
		bbox_y = bbox_top;	
	}
	
	//Down Slope
	if !place_meeting(x,y,objSlope) and vsp >= 0 
	and (place_meeting(x,y+2+abs(hsp),objSlope) or
	tilemap_get_at_pixel(tilemap, x, bbox_bottom+2+abs(hsp)) != 0)
	{
		if place_meeting(x,y+2+abs(hsp),objSlope){
			while !place_meeting(x,y+1,objSlope) {
				y += 1;
			}
		}else if tilemap_get_at_pixel(tilemap, x, bbox_bottom+2+abs(hsp)) != 0{
			while tilemap_get_at_pixel(tilemap, x, bbox_bottom+1) == 0 {
				y += 1;
			}	
		}
	}
	
	//Check tile on the top or bottom
	if tilemap_get_at_pixel(tilemap, bbox_right, bbox_y + vsp) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_left, bbox_y + vsp) != 0 {		
		
		if vsp >= 0 {
			for(i = bbox_left;i <= bbox_right;i++){
				if tilemap_get_at_pixel(tilemap, i, bbox_bottom + vsp+1) != 0 {
					var pixel = 0;
					while tilemap_get_at_pixel(tilemap, i, bbox_bottom+pixel) == 0 {
						pixel++;	
					}
					y += pixel-1;								
					break;	
				}	
			}		
			on_ground = true;
		}else{
			var i;
			for(i = bbox_left;i <= bbox_right;i++){
				if tilemap_get_at_pixel(tilemap, i, bbox_vertical + vsp) != 0 {
					var pixel = 0;
					while tilemap_get_at_pixel(tilemap, i, bbox_top-pixel) == 0 {
						pixel++;	
					}
					y -= pixel-1;
					break;	
				}
			}
		}
		vsp = 0;
	}
	
	//Vertical Collisionsd
	var slope = instance_place(x,y+vsp,objSlope);
	if slope >= 0{
		if vsp >= 0 {
			y = slope.y;
			while place_meeting(x,y,objSlope) y--;
			vsp = 0;
			on_ground = true;	
			if object_get_parent(self) == objBoss play_sfx(-1, sfx_boss_landing);
		}else{
			y = slope.y;
			while place_meeting(x,y,objSlope) y++;
			vsp = 0;
		}
	}
	
	y+=vsp;
	
	if tilemap_get_at_pixel(tilemap, bbox_right, bbox_bottom + 1) == 0 
	and tilemap_get_at_pixel(tilemap, bbox_left, bbox_bottom + 1) == 0 
	and tilemap_get_at_pixel(tilemap, x, bbox_bottom + 1) == 0
	and !place_meeting(x, y + 1, objSlope){
		on_ground = false	
	}
	
	if on_ground {
		vsp = 0;
	}else{
		vsp += grav;	
		if vsp >= 5 {
			vsp = 5;	
		}
	}		
}