// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerGoUpSlope(){
	if (place_meeting(x+hsp,y,objSlope)){
		//Up slope
		yplus=0;
		while place_meeting(x+hsp+sign(hsp),y-yplus,objSlope) && yplus <= abs(hsp) yplus+=1; //change the abs(hsp) to 3*abs(hsp) if you want to be able to go up a 3-1 slope, etc..
		if place_meeting(x+hsp,y-yplus,objSlope) and tilemap_get_at_pixel(tilemap, bbox_horizone + hsp, bbox_bottom) == 0{
		    while !place_meeting(x+sign(hsp),y,objSlope) x += sign(hsp);
		    hsp=0;
		}else{
		    y-=yplus;
		}
	}
}