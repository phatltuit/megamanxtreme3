// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function ZeroEndAttackAnimation(){	
	isAtk = false;	
	image_speed = 1;					
	sword.sprite_index = noone;
	canCombo = true;
	slashState = 0;	
	isRollAtk = false;
}