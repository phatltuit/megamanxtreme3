// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function draw_player_shadow(){
	if ((isDash and !isSkillAtk) or isHyperJump or isOverride) and !isBlocked{
		dash_shadow = part_type_create();
		part_system_depth(particle_sys, depth+1);
		part_type_sprite( dash_shadow,sprite_index, 1, 1 , 0);
		part_type_scale(dash_shadow, image_xscale, image_yscale);
		part_type_size(dash_shadow, 1, 0, 0, 0);
		part_type_life(dash_shadow, 20, 20);
		//part_type_alpha3(dash_shadow, 0.1, 0.1, 0.1);
		if is_Zero(){
			switch (suit_type) {
				case SuitType.Black:
				    part_type_color1(dash_shadow, c_black);
				    break;
					
				case SuitType.Virus:
					part_type_color1(dash_shadow, c_purple);
				    break;
					
				case SuitType.Normal:
					part_type_color1(dash_shadow, c_red);

			}
			
		}else{
			part_type_color1(dash_shadow, make_color_rgb(colorReplace[0].red, colorReplace[0].green, colorReplace[0].blue));
		}
		part_type_alpha1(dash_shadow, 0.1);
		
		if dashCounter mod 5 == 0 {
			part_particles_create(particle_sys, xprevious, yprevious, dash_shadow, 2);				
		}
	}
}