// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerPhysics(){
	if isOverride {
		runSpd = 2.5;
		climbSpd = 2.5;
		jumpSpd = 6.5;
		dashSpd = 4.5;
		hyperSpd = 4.5;
		dashCounterMax = (3.5*30)/4.5;
		hitSpd = 0;		
		chargeLv1 = 15;
		chargeLv2 = 55;
	}else{
		runSpd = 1.5;
		climbSpd = 1.5;
		jumpSpd = 5.5;
		dashSpd = 3.5;
		hyperSpd = 3.5;
		hitSpd = 0.5;
		dashCounterMax = 30;
		chargeLv1 = 30;
		chargeLv2 = 110;
	}
}