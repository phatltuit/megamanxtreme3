// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function SetHitEffLocation(playerAttack){
	bbox_hit = playerAttack.x > x ? bbox_right : bbox_left;
}