// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerAttackCancelAnimation(){	
	if is_Zero(){
		switch (state) {
		    case State.Crouch:
				if previous_state == State.CrouchAttack and key_down == 1{
				   image_index = sprite_get_number(sprCrouch) - 1;
				}
				break;
				
		    default:
		        // code here
		        break;
		}
	}
}