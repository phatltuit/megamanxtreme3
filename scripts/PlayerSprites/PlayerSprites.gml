// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerSprites(){
	switch (state) {
	    case State.StandAttack:		
			set_sprite(spriteAttack);
			break;
		case State.RunAttack:
			stepCounter++;
			if !audio_is_playing(sfx_walk) and stepCounter >= stepCounterMax {
				play_sfx(-1, sfx_walk);	
				stepCounter = 0;
			}
		case State.DashAttack:
		case State.JumpAttack:
		case State.ClimbAttack:
		case State.WallSlideAttack:		
		case State.CrouchAttack:
		case State.FlyAttack:
			set_sprite(spriteAttack);
			if is_Zero() {
				image_speed = slashSpd;				
				isAtk = true;			
				if isClimb {								
					sword.sprite_index = sprClimbAtkMask;	
				}else if isWallSlide {
					sword.sprite_index = sprWallSlideAtkMask;		
				}else if isCrouch {
					sword.sprite_index = sprCrouchAtkMask;
				}else if isRollAtk { 
					sword.sprite_index = sprRollAtkMask;
				}else{
					sword.sprite_index = sprJumpAtkMask;
				}
				sword.image_speed = image_speed;	
								
				if animation_end(){
					ZeroEndAttackAnimation();
					if !key_down {
						isCrouch = false;	
					}
				}
			}
		    break;
			
		case State.SkillAttack:
			set_sprite(spriteAttack);
			break;
			
		
		case State.Run:
			stepCounter++;
			if !audio_is_playing(sfx_walk) and stepCounter >= stepCounterMax {
				play_sfx(-1, sfx_walk);	
				stepCounter = 0;
			}
		case State.Stand:
			//stepCounter = 0;
		case State.Crouch:
			slashState = 0;
			set_sprite(spriteGround);
		    break;
			
		case State.Dash:
			slashState = 0;
			if on_ground {
				set_sprite(spriteGround);		   		
			}else{
				set_sprite(spriteAir);
			}
			break;
		
		case State.Jump:
		case State.Climb:	
		
		case State.WallSlide:
		case State.WallJump:
		case State.Fly:
			slashState = 0;
			set_sprite(spriteAir);
		    break;
			
		case State.ClimbUp:
			set_sprite(spriteAir);
			if animation_end(){
				frame_stop(image_number - 1);	
			}
		    break;
		
		    
		case State.Hit:
			set_sprite(spriteDamage);
		    break;
	        
	        
	    default:
	        // code here
	        break;
	}
}