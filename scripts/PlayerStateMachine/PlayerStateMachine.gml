// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function PlayerStateMachine(){
	if isPushBack {
		state = State.Hit;
		spriteDamage = sprHit;
	}else{
		if on_ground {
			if isAtk {
				if isDash {
					state = State.DashAttack;	
					previous_state = State.DashAttack;
					spriteAttack = sprDashAtk;	
				}else if isCrouch {
					state = State.CrouchAttack;
					previous_state = State.CrouchAttack;
					spriteAttack = sprCrouchAtk;					
				}else{
					if hsp != 0 {
						state = State.RunAttack
						previous_state = State.RunAttack
						spriteAttack = sprRunAtk;
					}else{ 
						state = State.StandAttack;	
						previous_state = State.StandAttack;
						spriteAttack = !is_Zero()?sprStandAtk[0] : sprStandAtk[slashState];
						if is_Zero(){
							sword.sprite_index = sprStandAtkMask[slashState];
						}
					}
				}
			}else if isSkillAtk{
			
			}else{
				if isDash {
					state = State.Dash;
					previous_state = State.Dash;
					spriteGround = sprDash;
				}else if isCrouch {		
					state = State.Crouch;
					PlayerAttackCancelAnimation();
					previous_state = State.Crouch;
					spriteGround = sprCrouch;
					image_index = animation_end()?image_number-1:image_index;	
				}else{
					if hsp != 0 {
						state = State.Run;	
						previous_state = State.Run;
						spriteGround = sprRun;
					}else{ 
						state = State.Stand;
						previous_state = State.Stand;
						spriteGround = sprStand;
					}
				}
			}
		}else{
			if isAtk {				
				if isClimb {
					state = State.ClimbAttack;
					previous_state = State.ClimbAttack;
					spriteAttack = sprClimbAtk;	
					if is_Zero(){sword.sprite_index = sprClimbAtkMask;}
				}else if isWallSlide {
					state = State.WallSlideAttack;
					previous_state = State.WallSlideAttack;
					spriteAttack = sprWallSlideAtk;		
					if is_Zero(){sword.sprite_index = sprWallSlideAtkMask;}
				}else{	
					if isDash {
						state = State.DashAttack;
						previous_state = State.DashAttack;
						spriteAttack = sprDashAtk;						
					}else{			
						state = State.JumpAttack;	
						previous_state = State.JumpAttack;	
						if isRollAtk{							
							spriteAttack = sprRollAtk;			
							if is_Zero(){sword.sprite_index = sprRollAtkMask;}
						}else{												
							if is_Zero(){sword.sprite_index = sprJumpAtkMask;}
							if vsp < 0 {
								spriteAttack = sprJumpAtk[0];
								image_index = animation_end()?image_number-1:image_index;
							}else{
								spriteAttack = sprJumpAtk[1];
								image_index = animation_end()?image_number-1:image_index;
							}
						}
					}
				}
				
			}else if isSkillAtk{
			
			}else{
				if isClimb {	
					if isGetUp {
						state = State.ClimbUp;
						spriteAir = sprClimbUp;
					}else{
						state = State.Climb;
						previous_state = State.Climb;
						spriteAir = sprClimb;
						if vsp != 0 {
							image_speed = 1;	
						}else{
							image_speed = 0;		
						}					
					}
				}else if isWallJump {
					state = State.WallJump;
					previous_state = State.WallJump;
					spriteAir = sprWallJump;		
				}else if isWallSlide {			
					state = State.WallSlide;
					previous_state = State.WallSlide;
					spriteAir = sprWallSlide;					
				}else{		
					if isDash {
						state = State.Dash;
						previous_state = State.Dash;
						spriteAir = sprDash;
					}else{
						state = State.Jump;		
						previous_state = State.Jump;		
						isWallJump = false;
						if !canDouleJump and is_Zero() and !canDash{
							if vsp <= 3 {
								spriteAir = sprJump[2];	
								image_index = animation_end()?2:image_index;								
							}else{
								spriteAir = sprJump[1];		
								image_index = animation_end()?image_number-1:image_index;
							}
						}else{
							if vsp < 0 {
								spriteAir = sprJump[0];	
								image_index = animation_end()?image_number-1:image_index;
							}else{
								spriteAir = sprJump[1];		
								image_index = animation_end()?image_number-1:image_index;
							}
						}
					}					
				}
			}
		}	
	}
}