// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function GroundSonicSlash(type, sonicType, delay_time, next_phase){
	if !phaseStart {
		image_index = 0;
		face_to_player();
		set_sprite(sprSlash[type]);
		phaseStart = true;
	}
	
	ContinueAction(spr_panther_got_hit, sprSlash[type], prev_image_index);
	
	if animation_end() {
		set_sprite(spr_panther_stand);		
	}else{
		if sprite_is(sprSlash[type]) {
			prev_image_index = image_index;
			dir = image_xscale == 1? 0 : 180;
			var i = 0;
			switch (type) {
			    case 0:
			        if image_index + image_speed == 3 {
						repeat 2 {
							play_sfx(-1, sfx_panther_slash);
							var atk = instance_create_depth(x, y-16, depth-100,objBossAtk);
							if sonicType == 3{
								if i == 0{
									set_sprite_of(atk, spr_panther_sonic_boom_3_a);	
								}else{
									set_sprite_of(atk, spr_panther_sonic_boom_3_b);	
								}
							}else{
								set_sprite_of(atk, sprSonicBoom[sonicType]);
							}
							var spd = 6;
							atk.alarm[0] = 180;
							atk.direction = dir + i*image_xscale;
							atk.hsp = spd*dcos(atk.direction);
							atk.vsp = dsin(atk.direction-180)*spd;
							atk.image_xscale = image_xscale;
							i+=30;
						}
					}
			        break;
					
				case 1:
			        if image_index + image_speed == 3 {
						play_sfx(-1, sfx_panther_slash);
						var atk = instance_create_depth(x, y-16, depth-100,objBossAtk);
						if sonicType == 3 {
							set_sprite_of(atk, spr_panther_sonic_boom_3_b);
						}else{
							set_sprite_of(atk, sprSonicBoom[sonicType]);
						}
						
						atk.alarm[0] = 60;
						var spd = 8;
						atk.direction = dir + 30*image_xscale;
						atk.hsp = spd*dcos(atk.direction);
						atk.vsp = dsin(atk.direction-180)*spd;
						atk.image_xscale = image_xscale;
					}
			        break;
					
				case 2:
			        if image_index + image_speed == 3 {
						play_sfx(-1, sfx_panther_slash);
						var atk = instance_create_depth(x, y-16, depth-100,objBossAtk);
						if sonicType == 3 {
							set_sprite_of(atk, spr_panther_sonic_boom_3_a);
						}else{
							set_sprite_of(atk, sprSonicBoom[sonicType]);
						}
						atk.alarm[0] = 60;
						var spd = 8;
						atk.direction = dir;
						atk.hsp = spd*dcos(atk.direction);
						atk.vsp = dsin(atk.direction-180)*spd;
						atk.image_xscale = image_xscale;
					}
			        break;
					
			    default:
			        // code here
			        break;
			}
		}
	}
	if sprite_is(spr_panther_stand){
		WaitForNextPhase(delay_time, next_phase);
	}
}

function AirSonicSlash(){
	var i = -45;
	repeat 3{
		play_sfx(-1, sfx_panther_slash);
		var atk = instance_create_depth(x+image_xscale*10, y, depth-100,objBossAtk);
		set_sprite_of(atk, spr_panther_sonic_boom_2);
		atk.alarm[0] = 60;
		var spd = 5;
		atk.direction = 270+i;
		atk.hsp = spd*dcos(atk.direction);
		atk.vsp = dsin(atk.direction-180)*spd;
		i+=45;
	}
}

