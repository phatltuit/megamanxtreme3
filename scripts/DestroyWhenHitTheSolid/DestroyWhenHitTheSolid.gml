// Script assets have changed for v2.3.0 see
// https://help.yoyogames.com/hc/en-us/articles/360005277377 for more information
function DestroyWhenHitTheSolid(){
	if (layer_tilemap_exists("Collision", tilemap) 
	and (tilemap_get_at_pixel(tilemap, bbox_right + hsp, bbox_bottom + vsp) != 0  
	or tilemap_get_at_pixel(tilemap, bbox_left + hsp, bbox_bottom + vsp) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_right + hsp, bbox_top + vsp) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_left + hsp, bbox_top + vsp) != 0 
	or tilemap_get_at_pixel(tilemap, x, bbox_bottom + vsp) != 0 
	or tilemap_get_at_pixel(tilemap, x, bbox_bottom + vsp) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_right + hsp, y) != 0 
	or tilemap_get_at_pixel(tilemap, bbox_left + hsp, y) != 0 
	or tilemap_get_at_pixel(tilemap, x , y) != 0))
	or place_meeting(x,y, objSlope){
		EnemyEffect(spr_enemy_small_hit, 1, x, y);
		instance_destroy();	
	}
	
}