//
// Simple passthrough fragment shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float flashColor;

void main()
{
    vec4 pixelColor = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	pixelColor.rbg = vec3(flashColor);
	gl_FragColor = pixelColor;
}
