/// @description Insert description here
// You can write your code in this editor
if global.isPaused exit;
var player = instance_find(objPlayer, 0);
if player >= 0{
	x = player.x;
	y = player.y;
	
	if sprite_index != noone{
		image_xscale = player.image_xscale;
		image_index = player.image_index;
		image_speed = player.image_speed;
	}
}
switch (get_sprite()) {	
    case spr_z_stand_slash_mask_0:	
	case spr_z_stand_slash_mask_1:
	case spr_z_stand_slash_mask_4:	
	case spr_z_stand_slash_mask_5:
		damage = 1 + (player.suit_type == SuitType.Black);
		
		break;
		
	case spr_z_crouch_slash:
	case spr_z_jump_slash_mask:
	case spr_z_wall_slide_slash_mask:
	case spr_z_climb_slash_mask:
	case spr_z_wall_slide_slash_mask_0:
	case spr_z_stand_slash_mask_2:	
	case spr_z_stand_slash_mask_6:	
		damage = 2 + (player.suit_type == SuitType.Black);
		break;
		
	case spr_z_roll_slash_mask:
		image_blend = c_red;
		damage = 2 + (player.suit_type == SuitType.Black);
		break;
	
	case spr_z_stand_slash_mask_3:
	case spr_z_skill_thunder_uppercut_mask:
	case spr_z_slash_tornado_mask:
	case spr_z_dash_mask:
		damage = 3 + (player.suit_type == SuitType.Black);
        break;
		
}