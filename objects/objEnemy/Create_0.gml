/// @description Insert description here
// You can write your code in this editor
damage = 2;
hp = 3;
maxHp = 3;

enum EnemyState {
	Active,
	DeadInside,
	DeadOutside
}

isDead = !in_view(self);
state = in_view(self)?EnemyState.Active : EnemyState.DeadOutside;




visible = in_view(self);

activateCounter = 0;
activateCounterMax = 10;

isHit = false;
hitCounter = 0;
hitCounterMax = 5;

hsp = 0;
vsp = 0;
spd = 0;