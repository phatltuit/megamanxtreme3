/// @description Insert description here
// You can write your code in this editor
if shouldBlock {
	image_speed = 0;
	exit;
}

var player = instance_place(x, y, objPlayer);

if player >= 0 and !shouldOpen and player.isReady{	
	image_speed = 1;	
	if door_transition_type == noone {
		if image_angle == 0 {
			if player.x < x {
				door_transition_type = Transition.Right;
			}else if player.x > x {
				door_transition_type = Transition.Left;	
			}
		}else{
			if player.y < y {
				door_transition_type = Transition.Down;			
			}else if player.y > y {
				door_transition_type = Transition.Up;	
			}	
		}
	}
	player.hsp = 0;
	player.vsp = 0;	
	player.isFreeze = true;
	player.ex_hsp = 32;
	player.transition_type = Transition.Door;	
	if animation_end() {
		audio_sound_gain(global.music, 0, 1000);
		frame_stop(image_number - 1)	
		player.transition_type = door_transition_type;
		shouldOpen = false;
		shouldClose = true;
	}	
}

if !shouldOpen and shouldClose and player < 0 {
	set_sprite(spr_door_close);
	var i = 0;
	repeat 3 {
		if image_angle == 0 {
			tilemap_set_at_pixel(tilemap,1, x, y+i);
			tilemap_set_at_pixel(tilemap,1, x+16, y+i);
		}else{
			tilemap_set_at_pixel(tilemap,1, x+i,y-32);
			tilemap_set_at_pixel(tilemap,1, x+i, y-16);	
		}
		i+=16;
	}
	if animation_end() {
		frame_stop(image_number - 1)	
		shouldBlock = true;			
	}
}
