/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
if global.isPaused exit;
event_inherited();
var player = instance_find(objPlayer, 0);
if !isDead and player >= 0 and player.isReady and !player.isFreeze{
	direction = point_direction(x,y, objPlayer.x, objPlayer.y);
	speed = 0.8;
}else{
	speed = 0;	
}
