/// @description Insert description here
// You can write your code in this editor
var player = instance_place(x, y, objPlayer);
if player >= 0 and ceil(player.x) == ceil(x) and player.vsp >= 0{
	player.y = y;
	while place_meeting(x, y, player){
		player.y--;	
	}
	player.on_ground = true;
	player.vsp = 0;
}