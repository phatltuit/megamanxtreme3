/// @description Insert description here
// You can write your code in this editor
if sprite_is(spr_x_charge_aura_lv1){
	image_speed = 1;
	draw_sprite_ext(sprite_index, image_index,x,y,0.6, 0.6, image_angle, image_blend, image_alpha);
}else if sprite_is(spr_x_charge_aura_lv2){
	image_speed = 1.6;
	draw_sprite_ext(spr_x_charge_aura_lv1, image_index,x,y,0.6, 0.6, image_angle, image_blend, image_alpha);
	draw_sprite_ext(spr_x_charge_aura_lv2, image_index,x,y,0.6, 0.6, image_angle, image_blend, image_alpha);
}