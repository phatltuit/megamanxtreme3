/// @description Insert description here
// You can write your code in this editor
if instance_exists(objPlayer){
	depth = objPlayer.depth - 1;
	x = objPlayer.x + objPlayer.image_xscale*2;
	y = objPlayer.y-6;
	image_xscale = objPlayer.image_xscale;
	if objPlayer.isBlocked {
		objPlayer.chargeCounter = 0;
		sprite_index = noone;
	}
}else{
	instance_destroy();	
}