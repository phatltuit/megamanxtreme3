/// @description Insert description here
// You can write your code in this editor
if isReady and !isBlocked and !global.isPaused{
	var Z = instance_create_depth(x,y, -100, objZ);
	Z.beginCounter = Z.beginCounterMax;
	instance_destroy(objPlayerCharge);
	instance_destroy(objPlayerEffect);	
	instance_destroy();
}