/// @description Insert description here
// You can write your code in this editor
if global.isPaused exit;
switch (sprite_index) {
    case spr_enemy_small_hit:
        if canPlaySFX {
			canPlaySFX = false;
			play_sfx(-1, sfx_enemy_damage);
		}
		
        break;
		
	case spr_enemy_resist:
        if canPlaySFX {
			canPlaySFX = false;
			play_sfx(-1, sfx_enemy_resist);
		}
		
        break;
		
	case spr_enemy_small_explosion:
        //Explosion sound
		if canPlaySFX {
			canPlaySFX = false;
			play_sfx(-1, sfx_enemy_small_explosion);
		}
        break;
		
	case spr_boss_explosion:
		if canPlaySFX {
			canPlaySFX = false;
			play_sfx(-1, sfx_enemy_small_explosion);
		}
		break;
	
    default:
        // code here
        break;
}