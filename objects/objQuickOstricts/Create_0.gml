/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

hp = 0;
state = BossState.Deactive;
sprPose = spr_ostrict_pose;
sprHit = spr_ostrict_got_hit;
sprRush = spr_ostrict_rush;
sprStand = spr_ostrict_stand;
sprJumpAtk = spr_ostrict_jump_atk;
sprJump[0] = spr_ostrict_jump_0;
sprLanding = spr_ostrict_landing;
sprRangeAtk = spr_ostrict_flame_shoot;


ColorClass();
sh_handle_range = shader_get_uniform(sh_color_replace, "range");
//Color Palete
colorMatch[0] = new Color(0,248,64);
colorMatch[1] = new Color(0,200,40);
colorMatch[2] = new Color(0,144,24);	
colorMatch[3] = new Color(88,104,232);
colorMatch[4] = new Color(80,64,176);


colorReplace[0] = new Color(248,248,144);
colorReplace[1] = new Color(248,224,0);
colorReplace[2] = new Color(248,160,0);
colorReplace[3] = new Color(150,24,0);
colorReplace[4] = new Color(84,24,0);

color_palete_lenght = array_length(colorReplace);
for (var i = 0; i < color_palete_lenght; ++i) {
	sh_handle_match[i] = shader_get_uniform(sh_color_replace, "colorMatch"+string(i));
	sh_handle_replace[i] = shader_get_uniform(sh_color_replace, "colorReplace"+string(i));    
}


