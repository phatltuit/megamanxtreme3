/// @description Insert description here
// You can write your code in this editor
//state = BossState.Active;
if global.isPaused exit;
event_inherited();
switch (state) {
    case BossState.Active:
		//hsp = (keyboard_check(ord("D")) - keyboard_check(ord("A"))) * 2;
		BossPushBack();
		
		if isPushBack exit;
		
		switch (phase) {
			case 0:
				canPushBack = false;
				mask_index = spr_ostrict_run_mask;	
				if hsp != 0 and ceil(rushCounter) mod 4 == 0{
					var atk = instance_create_depth(x, y, depth, objBossAtk);
					atk.alarm[0] = 60;
					set_sprite_of(atk, spr_ostrict_ground_flame);
					atk.damage = 3;
					atk.image_xscale = image_xscale;
				}
				RushToPlayer(DistanceFromPlayer()*2,5, 60, 1);				
				break;
				
			case 1:
				canPushBack = false;	
				mask_index = spr_ostrict_mask;				
				if !phaseStart{
					face_to_player();
					phaseStart = true;
					BossSimpleJump(DistanceFromPlayer()*2,90, image_xscale);
				}
				prev_image_index = image_index;				
				if !on_ground{					
					ContinueAction(sprHit, sprJumpAtk, prev_image_index);
					jumpCounter--;
					if jumpCounter <= jumpCounterHalf and jumpCounter > jumpCounterHalf-15{
						hsp = 0;
						vsp = 0;
						set_sprite(sprJumpAtk);
					}
					
					if sprite_is(sprJumpAtk) {
						vsp = 7;
					}
					
					if animation_end(){ 
						frame_stop(image_number - 1)	
					}
				}else{
					ContinueAction(sprHit, sprLanding, prev_image_index);
					if !sprite_is(sprLanding){
						set_sprite(sprLanding);	
						if instance_exists(objPlayer) {
							if (objPlayer.on_ground or objPlayer.isWallSlide)
							and (!objPlayer.isSkillAtk and global.weaponIndex != 8) and !objPlayer.isHit {
								//objPlayer.isHit = true;	
								objPlayer.isPushBack = true;
							} 
							var i =  1;
							repeat 2 {											
								var atk = instance_create_depth(x, y-2, depth-1, objBossAtk);
								atk.alarm[0] = 60;
								set_sprite_of(atk, spr_ostrict_ground_flame_wave);
								atk.damage = 3;
								atk.hsp = 3*i;
								atk.image_xscale = i;	
								i = -1;
							}
						}
						
					}
					if instance_exists(objPlayer){
						if !objPlayer.isDead and objPlayer.isReady{
							camera_set_view_pos(view_camera[0], get_view_x(), get_view_y()+irandom(5));
						}
					}
					if animation_end(){ 
						frame_stop(image_number - 1)	
						WaitForNextPhase(90, 2);	
					}
				}
	
				break;
				
			case 2:
				if !phaseStart {
					face_to_player();
					BossSimpleJump(0, 60,image_xscale);
					set_sprite(sprRangeAtk);
					phaseStart = true;
				}				
						
				if sprite_is(sprRangeAtk) {					
					if instance_number(objBossAtk) < 1 and image_index + image_speed >= 10 {
						grav = 0;
						vsp = 0;
						
						var atk = instance_create_depth(bbox_xscale(), y-26, depth-1, objBossAtk);
						set_sprite_of(atk, spr_ostrict_flame_circle);
						atk.damage = 3;
						atk.alarm[0] = 180;
						//atk.image_xscale = image_xscale;
						if instance_exists(objPlayer){
							atk.direction = point_direction(atk.x,atk.y, objPlayer.x, objPlayer.y);
							var spd = 3;
							atk.hsp = dcos(atk.direction)*spd;
							atk.vsp = dsin(atk.direction-180)*spd;
							atk.alarm[1] = point_distance(atk.x,atk.y, objPlayer.x, objPlayer.y)/spd;
						}else{
							atk.hsp = 5*image_xscale;
						}								
					}
								
					if animation_end() {
						grav = 0.25;
						set_sprite(sprJumpAtk);	
					}
				}
					
				if on_ground {
					set_sprite(sprLanding);
					if animation_end(){
						frame_stop(image_number - 1);	
					}
					WaitForNextPhase(60, 0);	
				}
				
				break;
			
			
			default:
				break;
		}	
		BossCollision();				
        break;
		
	case BossState.Defeated:		
		BossDefeated();
		break;
		
	case BossState.Deactive:
        // code here
        break;
		
	case BossState.Begin:
		BossBegin()
        break;
}