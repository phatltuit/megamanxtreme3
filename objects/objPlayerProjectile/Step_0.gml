/// @description Insert description here
// You can write your code in this editor
switch (sprite_index) {
	case spr_ba_buster_lv1:
	case spr_x_buster_lv1:
        damage = 1;
		DestroyWhenHitTheSolid();
		DestroyWhenHitEnemy();
        break;
		
	case spr_ba_buster_lv2:
	case spr_x_buster_lv2:
	case spr_ma_buster_lv2:
		damage = 2;
		DestroyWhenHitTheSolid();
		DestroyWhenHitEnemy();
        break;
		
    case spr_x_buster_lv3:
        damage = 3;
		DestroyWhenHitTheSolid();
		DestroyWhenHitEnemy();
        break;
	
	case spr_ba_buster_lv3:
		damage = 3;
		DestroyWhenHitEnemy();
		break;
	
	case spr_ma_buster_lv3:		
        damage = 3;
		var enemy = instance_place(x,y,objEnemy);
		if enemy >= 0 {
			if instance_number(objPlayerProjectileExplosion) < 1{
				var yy;
				if y > enemy.y {
					if y < enemy.bbox_top {
						yy = enemy.bbox_top+(bbox_bottom - enemy.bbox_top)/2;
					}else{
						yy = y;	
					}
				}else{
					if y > enemy.bbox_bottom {
						yy = enemy.bbox_top - (enemy.bbox_bottom + bbox_top)/2;
					}else{
						yy = y;	
					}	
				}								
				expl = instance_create_depth(enemy.x, yy , enemy.depth-1, objPlayerProjectileExplosion);
				expl.sprite_index = spr_ma_buster_lv3_explosion;	
				expl.alarm[0] = 121;
			}
		}
		
		break;
		
	case spr_z_skill_sonic_burst:
		damage = 5;
		
		break;
		
	case spr_ma_nova_strike_1:
		if instance_exists(objPlayer){
			x = objPlayer.x;
			y = objPlayer.y;
		}else{
			instance_destroy();	
		}
		damage = 5;
		break;

	case spr_ma_buster_lv3_explosion:
		damage = 1;
		break;
		
	case spr_x_thunder_shock:
		DestroyWhenHitTheSolid();
		if place_meeting(x,y, objEnemy){
			instance_destroy();	
		}
		x+=hsp;
		damage = 3;		
		break;
		
	case spr_z_thunder_uppercut_eff:
		//PlayerProjectileCollision();
		//DestroyWhenHitTheSolid();		
		vsp += 0.25;
		y += vsp;
		damage = 1;	
		break;
		
	case spr_z_skill_tornado:
		x+=hsp;
		damage = 3;	
		break;
		
	
		
    default:
        // code here
        break;
}