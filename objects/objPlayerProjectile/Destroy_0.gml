/// @description Insert description here
// You can write your code in this editor
if sprite_is(spr_x_thunder_shock) {
	var i = 45;	
	repeat 4 {
		var atk = instance_create_depth(x, y, depth - 1, objPlayerProjectileLv2);
		atk.speed = 3.5;
		atk.direction = i
		atk.image_angle = i;
		set_sprite_of(atk, spr_x_thunder_shock_eff);	
		i += 90;
	}
}