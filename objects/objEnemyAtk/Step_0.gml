/// @description Insert description here
// You can write your code in this editor
if global.isPaused exit;
switch (sprite_index) {
    case spr_panther_sonic_boom_0:
		ProjectileReflect();
		damage = 1;
        break;
	case spr_panther_sonic_boom_1:
	case spr_panther_sonic_boom_2:
		damage = 1;
        break;
	case spr_panther_sonic_boom_3_a:	
	case spr_panther_sonic_boom_3_b:
	case spr_panther_sonic_boom_3_c:
		x+=hsp;
		y+=vsp;
		damage = 2;
		break;
		
	case spr_ostrict_ground_flame:
	case spr_ostrict_ground_flame_wave:	
		BossAtkCollision();		
		damage = 2;
		break;
		
	case spr_ostrict_flame_circle:
		mask_index = spr_ostrict_flame_circle; 		
		if isSpread {	
			BossAtkCollision();
			if hsp != 0 and ceil(rushCounter) mod 4 == 0{
					var atk = instance_create_depth(x, y, depth, objBossAtk);
					atk.alarm[0] = 60;
					set_sprite_of(atk, spr_ostrict_ground_flame);
					atk.damage = 3;
					atk.image_xscale = image_xscale;
			}
			if !phaseStart {
				face_to_player();	
				var rushSpd = 6;					
				hsp = rushSpd*image_xscale;
				rushCounter = DistanceFromPlayer()*3/rushSpd;
				phaseStart = true;
			}
			if rushCounter > 0 {
				rushCounter--;		
			}else{
				image_alpha--;
				if image_alpha <= 0.2 {
					instance_destroy();		
				}
				
			}
		}else{
			BossAtkCollision();
			//x+=hsp;
			//y+=vsp;
		}
		damage = 2;
		break;
		
    default:
        // code here
        break;
}