/// @description Insert description here
// You can write your code in this editor
//state = BossState.Active;
if global.isPaused exit;
event_inherited();
switch (state) {
    case BossState.Active:
		//hsp = (keyboard_check(ord("D")) - keyboard_check(ord("A"))) * 2;
		BossPushBack();
		
		if isPushBack exit;
		
		switch (phase) {
			case 0:
				PounceOnPlayer(0,130, 60, 1, true);
				break;
				
			case 1:
				GroundSonicSlash(1, 3,30, 2);
				break;
				
			case 2:
				RushToPlayer(DistanceFromPlayer(),6, 10, 3);		
				break;
				
			case 3:
				GroundSonicSlash(2, 3,30, 4);
				break;
				
			case 4:
				Jumpback(1, 1, 46, -image_xscale, 60,5);
				break;
				
			case 5:				
				GroundSonicSlash(1, 3,10, 6);
				break;
			
			case 6:				
				GroundSonicSlash(2, 3,20, 7);
				break;
				
			case 7:				
				GroundSonicSlash(0, 3,30, 8);
				break;
				
			case 8:
				PounceOnPlayer(0,50, 20, 9, false);
				break;				
				
			case 9:
				Jumpback(1, abs(get_view_center_x()-x), 140, x > get_view_center_x()?-1:1, 5,10);
				break;
			
			case 10:
				SonicBurst();									
				break;
				
		    default:
		        break;
		}	
		BossCollision();				
        break;
		
	case BossState.Defeated:		
		BossDefeated();
		break;
		
	case BossState.Deactive:
        // code here
        break;
		
	case BossState.Begin:
		BossBegin();
        break;
}