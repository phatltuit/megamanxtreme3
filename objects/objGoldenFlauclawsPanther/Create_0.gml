/// @description Insert description here
// You can write your code in this editor

// Inherit the parent event
event_inherited();

hp = 0;
state = BossState.Deactive;
sprPose = spr_panther_pose;
sprRush = spr_panther_quick_atk;
sprJump[0] = spr_panther_jump_0;
sprHit = spr_panther_got_hit;
sprStand = spr_panther_stand;

sprSlash[0] = spr_panther_ground_slash_0;
sprSlash[1] = spr_panther_ground_slash_1;
sprSlash[2] = spr_panther_ground_slash_1;

sprSonicBoom[0] = spr_panther_sonic_boom_0;
sprSonicBoom[1] = spr_panther_sonic_boom_1;
sprSonicBoom[2] = spr_panther_sonic_boom_2;

ColorClass();
sh_handle_range = shader_get_uniform(sh_color_replace, "range");
//Color Palete
colorMatch[0] = new Color(136,144,168);
colorMatch[1] = new Color(104,120,120);
colorMatch[2] = new Color(72,96,104);	
colorMatch[3] = new Color(56,72,88);
colorMatch[4] = new Color(240,240,240);

colorReplace[0] = new Color(248,248,144);
colorReplace[1] = new Color(248,224,0);
colorReplace[2] = new Color(248,160,0);
colorReplace[3] = new Color(184,104,0);
colorReplace[4] = new Color(248,248,144);

color_palete_lenght = array_length(colorReplace);
for (var i = 0; i < color_palete_lenght; ++i) {
	sh_handle_match[i] = shader_get_uniform(sh_color_replace, "colorMatch"+string(i));
	sh_handle_replace[i] = shader_get_uniform(sh_color_replace, "colorReplace"+string(i));    
}


