{
  "compression": 0,
  "volume": 1.0,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_panther_slash.wav",
  "duration": 0.594206,
  "parent": {
    "name": "QuickPanther",
    "path": "folders/Sounds/SFX/Boss/QuickPanther.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_panther_slash",
  "tags": [],
  "resourceType": "GMSound",
}