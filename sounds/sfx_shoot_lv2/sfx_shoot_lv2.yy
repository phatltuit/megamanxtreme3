{
  "compression": 0,
  "volume": 0.8,
  "preload": false,
  "bitRate": 128,
  "sampleRate": 44100,
  "type": 0,
  "bitDepth": 1,
  "audioGroupId": {
    "name": "audiogroup_default",
    "path": "audiogroups/audiogroup_default",
  },
  "soundFile": "sfx_shoot_lv2.wav",
  "duration": 0.536156,
  "parent": {
    "name": "SFX",
    "path": "folders/Sounds/SFX.yy",
  },
  "resourceVersion": "1.0",
  "name": "sfx_shoot_lv2",
  "tags": [],
  "resourceType": "GMSound",
}