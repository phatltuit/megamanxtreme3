{
  "bboxMode": 0,
  "collisionKind": 0,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 5,
  "bbox_right": 37,
  "bbox_top": 1,
  "bbox_bottom": 46,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 38,
  "height": 48,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f39039f8-95db-4bdf-8bad-71ae395eb937","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f39039f8-95db-4bdf-8bad-71ae395eb937","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"LayerId":{"name":"9dd656c4-e39c-42dc-96a3-1728e70d3347","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_panther_sonic_boom_3_a","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"resourceVersion":"1.0","name":"f39039f8-95db-4bdf-8bad-71ae395eb937","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"02f193de-5f7c-4bde-881b-a318e7416e9f","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"02f193de-5f7c-4bde-881b-a318e7416e9f","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"LayerId":{"name":"9dd656c4-e39c-42dc-96a3-1728e70d3347","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_panther_sonic_boom_3_a","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"resourceVersion":"1.0","name":"02f193de-5f7c-4bde-881b-a318e7416e9f","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_panther_sonic_boom_3_a","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"6e21846a-c398-42c3-b88a-34c2c71d1de4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f39039f8-95db-4bdf-8bad-71ae395eb937","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf2ec988-d4dc-4090-b914-a30db0c0fbcf","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"02f193de-5f7c-4bde-881b-a318e7416e9f","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 20,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_panther_sonic_boom_3_a","path":"sprites/spr_panther_sonic_boom_3_a/spr_panther_sonic_boom_3_a.yy",},
    "resourceVersion": "1.3",
    "name": "spr_panther_sonic_boom_3_a",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"9dd656c4-e39c-42dc-96a3-1728e70d3347","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "QuickPanther",
    "path": "folders/Sprites/Boss/QuickPanther.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_panther_sonic_boom_3_a",
  "tags": [],
  "resourceType": "GMSprite",
}