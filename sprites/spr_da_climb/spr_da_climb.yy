{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 23,
  "bbox_top": 0,
  "bbox_bottom": 48,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 24,
  "height": 49,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": [
    4278190335,
    4278255615,
    4278255360,
    4294967040,
    4294901760,
    4294902015,
    4293974136,
    4293717228,
    4293059298,
    4292335575,
    4291677645,
    4290230199,
    4287993237,
    4280556782,
    4278252287,
    4283540992,
    4293963264,
    4287770926,
    4287365357,
    4287203721,
    4286414205,
    4285558896,
    4284703587,
    4283782485,
    4281742902,
    4278190080,
    4286158839,
    4286688762,
    4287219453,
    4288280831,
    4288405444,
    4288468131,
    4288465538,
    4291349882,
    4294430829,
    4292454269,
    4291466115,
    4290675079,
    4290743485,
    4290943732,
    4288518390,
    4283395315,
    4283862775,
    4284329979,
    4285068799,
    4285781164,
    4285973884,
    4286101564,
    4290034460,
    4294164224,
    4291529796,
    4289289312,
    4289290373,
    4289291432,
    4289359601,
    4286410226,
    4280556782,
    4280444402,
    4280128760,
    4278252287,
    4282369933,
    4283086137,
    4283540992,
    4288522496,
    4293963264,
    4290540032,
    4289423360,
    4289090560,
    4287770926,
    4287704422,
    4287571858,
    4287365357,
    4284159214,
    4279176094,
    4279058848,
    4278870691,
    4278231211,
    4281367321,
  ],
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"a344ee4a-1978-4080-ac70-356b5de40741","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a344ee4a-1978-4080-ac70-356b5de40741","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":{"name":"b44b0df8-06c7-4755-870f-f1527585ed12","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_da_climb","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"a344ee4a-1978-4080-ac70-356b5de40741","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f2822e12-7bed-404e-b9e6-9eae308dfe95","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f2822e12-7bed-404e-b9e6-9eae308dfe95","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":{"name":"b44b0df8-06c7-4755-870f-f1527585ed12","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_da_climb","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"f2822e12-7bed-404e-b9e6-9eae308dfe95","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e3df7e35-3255-4e6c-a9cc-aad7956974b8","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e3df7e35-3255-4e6c-a9cc-aad7956974b8","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":{"name":"b44b0df8-06c7-4755-870f-f1527585ed12","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_da_climb","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"e3df7e35-3255-4e6c-a9cc-aad7956974b8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"eb9d1108-41ac-4161-9891-ed0d9779cb27","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"eb9d1108-41ac-4161-9891-ed0d9779cb27","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":{"name":"b44b0df8-06c7-4755-870f-f1527585ed12","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_da_climb","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"eb9d1108-41ac-4161-9891-ed0d9779cb27","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"24304f59-335c-44cb-9c6c-65eff80f65bb","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"24304f59-335c-44cb-9c6c-65eff80f65bb","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":{"name":"b44b0df8-06c7-4755-870f-f1527585ed12","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_da_climb","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"24304f59-335c-44cb-9c6c-65eff80f65bb","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7038f9b0-00a6-401b-be50-809e51876251","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7038f9b0-00a6-401b-be50-809e51876251","path":"sprites/spr_da_climb/spr_da_climb.yy",},"LayerId":{"name":"b44b0df8-06c7-4755-870f-f1527585ed12","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_da_climb","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","name":"7038f9b0-00a6-401b-be50-809e51876251","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_da_climb","path":"sprites/spr_da_climb/spr_da_climb.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 20.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 6.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"cefd5bab-ff89-4973-a33c-59fc831f5580","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a344ee4a-1978-4080-ac70-356b5de40741","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00df4848-75d6-4681-9bd0-9679f2903008","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f2822e12-7bed-404e-b9e6-9eae308dfe95","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ee541530-f2f9-4ad3-8c26-9017bb51b0e3","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e3df7e35-3255-4e6c-a9cc-aad7956974b8","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cc9cd384-9745-4d33-b8e6-1c1f6e9942e2","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"eb9d1108-41ac-4161-9891-ed0d9779cb27","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f06dd54a-9f4f-4efb-97a3-488a7e933e2e","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"24304f59-335c-44cb-9c6c-65eff80f65bb","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d317f8d7-9018-49ba-81af-a53e5e5ddf89","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7038f9b0-00a6-401b-be50-809e51876251","path":"sprites/spr_da_climb/spr_da_climb.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 12,
    "yorigin": 31,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_da_climb","path":"sprites/spr_da_climb/spr_da_climb.yy",},
    "resourceVersion": "1.3",
    "name": "spr_da_climb",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"b44b0df8-06c7-4755-870f-f1527585ed12","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Delta",
    "path": "folders/Sprites/Player/X/Armor/Delta.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_da_climb",
  "tags": [],
  "resourceType": "GMSprite",
}