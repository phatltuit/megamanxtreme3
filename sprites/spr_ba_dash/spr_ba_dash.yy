{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 42,
  "bbox_top": 1,
  "bbox_bottom": 33,
  "HTile": false,
  "VTile": false,
  "For3D": true,
  "width": 50,
  "height": 34,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 4,
  "gridY": 16,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"2209a4fd-14d6-4e87-b06b-d1ea7045ed5b","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2209a4fd-14d6-4e87-b06b-d1ea7045ed5b","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"2209a4fd-14d6-4e87-b06b-d1ea7045ed5b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ff6c8ba6-5da9-40e7-905c-ecd1f2a5dcd0","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ff6c8ba6-5da9-40e7-905c-ecd1f2a5dcd0","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"ff6c8ba6-5da9-40e7-905c-ecd1f2a5dcd0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b5e805ae-233d-4475-aa15-84d0392dcfae","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b5e805ae-233d-4475-aa15-84d0392dcfae","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"b5e805ae-233d-4475-aa15-84d0392dcfae","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"92bb43b3-3d5e-410f-9dfd-4f03a20daa31","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"92bb43b3-3d5e-410f-9dfd-4f03a20daa31","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"92bb43b3-3d5e-410f-9dfd-4f03a20daa31","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dd4f5fa3-c395-43e9-8965-c6d04de1e020","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dd4f5fa3-c395-43e9-8965-c6d04de1e020","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"dd4f5fa3-c395-43e9-8965-c6d04de1e020","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"db2b6a31-6d1f-4a82-91a8-65772605e2af","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"db2b6a31-6d1f-4a82-91a8-65772605e2af","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"db2b6a31-6d1f-4a82-91a8-65772605e2af","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9db92906-ac4f-40a4-b4f2-eca85a56dfdc","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9db92906-ac4f-40a4-b4f2-eca85a56dfdc","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"9db92906-ac4f-40a4-b4f2-eca85a56dfdc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5a46094a-9328-4bb2-8e96-f92297cfbe75","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5a46094a-9328-4bb2-8e96-f92297cfbe75","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"5a46094a-9328-4bb2-8e96-f92297cfbe75","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9280627e-169f-4f68-bced-9ec2a4982550","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9280627e-169f-4f68-bced-9ec2a4982550","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"9280627e-169f-4f68-bced-9ec2a4982550","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"867bb877-2d36-4a10-a651-aa6be8ea1fb1","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"867bb877-2d36-4a10-a651-aa6be8ea1fb1","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"867bb877-2d36-4a10-a651-aa6be8ea1fb1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7a793af6-3d4e-4870-b536-a1cf16e50573","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7a793af6-3d4e-4870-b536-a1cf16e50573","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"LayerId":{"name":"955f8033-47f7-43e5-ab26-226560f38af5","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","name":"7a793af6-3d4e-4870-b536-a1cf16e50573","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 18.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 11.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3b1a5349-cb90-4735-8323-806a6587903f","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2209a4fd-14d6-4e87-b06b-d1ea7045ed5b","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8e2030bf-e242-4c8b-9105-67311cd5f324","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ff6c8ba6-5da9-40e7-905c-ecd1f2a5dcd0","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"386329c5-50c4-491b-ac84-fe56f048d344","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b5e805ae-233d-4475-aa15-84d0392dcfae","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bfb6eb0b-0543-43f9-9129-5036b3356e8d","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"92bb43b3-3d5e-410f-9dfd-4f03a20daa31","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2fda0142-e357-4217-abcf-6d1041793ca2","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dd4f5fa3-c395-43e9-8965-c6d04de1e020","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d214315f-db68-4f1a-a5be-bd41fe05f46f","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"db2b6a31-6d1f-4a82-91a8-65772605e2af","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1c8809b9-baa8-43c8-bcc6-514cfef8e16e","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9db92906-ac4f-40a4-b4f2-eca85a56dfdc","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"02f86594-67a6-425b-ac9f-96db3dd3cef3","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5a46094a-9328-4bb2-8e96-f92297cfbe75","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"00f45d87-86b7-4ad0-bb34-d1b8c8489787","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9280627e-169f-4f68-bced-9ec2a4982550","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a1691b85-cdfa-4b49-853c-6d301b5ed859","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"867bb877-2d36-4a10-a651-aa6be8ea1fb1","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cedf9deb-5ac9-4f54-ae02-56b3067975c5","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a793af6-3d4e-4870-b536-a1cf16e50573","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 28,
    "yorigin": 24,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ba_dash","path":"sprites/spr_ba_dash/spr_ba_dash.yy",},
    "resourceVersion": "1.3",
    "name": "spr_ba_dash",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"955f8033-47f7-43e5-ab26-226560f38af5","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Beta",
    "path": "folders/Sprites/Player/X/Armor/Beta.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ba_dash",
  "tags": [],
  "resourceType": "GMSprite",
}