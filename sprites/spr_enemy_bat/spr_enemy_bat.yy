{
  "bboxMode": 2,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 10,
  "bbox_right": 28,
  "bbox_top": 8,
  "bbox_bottom": 22,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 39,
  "height": 26,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e09c8f40-508c-49b7-bd10-baa620179165","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e09c8f40-508c-49b7-bd10-baa620179165","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":{"name":"a41add16-3c3e-4253-ba20-35866a6ae16c","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_enemy_bat","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"e09c8f40-508c-49b7-bd10-baa620179165","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"00e7928d-7b0b-4d4e-9f26-f2230fb0b616","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"00e7928d-7b0b-4d4e-9f26-f2230fb0b616","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":{"name":"a41add16-3c3e-4253-ba20-35866a6ae16c","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_enemy_bat","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"00e7928d-7b0b-4d4e-9f26-f2230fb0b616","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b0239a9b-4709-4f76-a82a-7394656e9089","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b0239a9b-4709-4f76-a82a-7394656e9089","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":{"name":"a41add16-3c3e-4253-ba20-35866a6ae16c","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_enemy_bat","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"b0239a9b-4709-4f76-a82a-7394656e9089","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"65d67211-9468-45bc-8fb3-4f26a32b2364","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"65d67211-9468-45bc-8fb3-4f26a32b2364","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":{"name":"a41add16-3c3e-4253-ba20-35866a6ae16c","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_enemy_bat","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"65d67211-9468-45bc-8fb3-4f26a32b2364","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"63be6207-75cb-4567-a8a4-7d6e2b2bb08d","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"63be6207-75cb-4567-a8a4-7d6e2b2bb08d","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"LayerId":{"name":"a41add16-3c3e-4253-ba20-35866a6ae16c","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_enemy_bat","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","name":"63be6207-75cb-4567-a8a4-7d6e2b2bb08d","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_enemy_bat","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 20.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 5.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a00d00e3-6953-462b-b5ad-63926cfcde33","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e09c8f40-508c-49b7-bd10-baa620179165","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ac970e50-2f2e-4afb-81ab-9d5d37d9edd8","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"00e7928d-7b0b-4d4e-9f26-f2230fb0b616","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"472af72b-a458-4801-ab64-237e882f88e5","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b0239a9b-4709-4f76-a82a-7394656e9089","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ea5e24cf-80f2-4ba8-808c-e3497b85e782","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"65d67211-9468-45bc-8fb3-4f26a32b2364","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b249f989-3424-4b7d-b515-831a94dfc6bb","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"63be6207-75cb-4567-a8a4-7d6e2b2bb08d","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 19,
    "yorigin": 16,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_enemy_bat","path":"sprites/spr_enemy_bat/spr_enemy_bat.yy",},
    "resourceVersion": "1.3",
    "name": "spr_enemy_bat",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"a41add16-3c3e-4253-ba20-35866a6ae16c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Enemy",
    "path": "folders/Sprites/Enemy.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_enemy_bat",
  "tags": [],
  "resourceType": "GMSprite",
}