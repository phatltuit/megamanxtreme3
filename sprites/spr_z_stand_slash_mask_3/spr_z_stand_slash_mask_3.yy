{
  "bboxMode": 0,
  "collisionKind": 4,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 117,
  "bbox_top": 0,
  "bbox_bottom": 86,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 118,
  "height": 90,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 4,
  "gridY": 4,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"bd058358-b81c-4ddf-a581-813faffb46ae","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"bd058358-b81c-4ddf-a581-813faffb46ae","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"bd058358-b81c-4ddf-a581-813faffb46ae","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"42ad9958-ea32-4718-9ea9-8fb25d2764b5","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"42ad9958-ea32-4718-9ea9-8fb25d2764b5","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"42ad9958-ea32-4718-9ea9-8fb25d2764b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6ece9dc2-d12e-4b8a-a633-1785a38ed0c7","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6ece9dc2-d12e-4b8a-a633-1785a38ed0c7","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"6ece9dc2-d12e-4b8a-a633-1785a38ed0c7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77d8a375-7aca-4759-9e59-b6d57867f632","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77d8a375-7aca-4759-9e59-b6d57867f632","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"77d8a375-7aca-4759-9e59-b6d57867f632","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dcd1c70e-e569-4f7a-ad51-a19750e3a157","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dcd1c70e-e569-4f7a-ad51-a19750e3a157","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"dcd1c70e-e569-4f7a-ad51-a19750e3a157","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"560de937-7562-45fe-9959-c819c0db2434","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"560de937-7562-45fe-9959-c819c0db2434","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"560de937-7562-45fe-9959-c819c0db2434","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5129112a-5ba9-4f0d-b693-6febc0004cf0","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5129112a-5ba9-4f0d-b693-6febc0004cf0","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"5129112a-5ba9-4f0d-b693-6febc0004cf0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ab28b515-b5ee-49d1-93b5-7161f29a7d15","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ab28b515-b5ee-49d1-93b5-7161f29a7d15","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"ab28b515-b5ee-49d1-93b5-7161f29a7d15","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b8133605-fcee-4be6-a821-64548ae9ecd3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b8133605-fcee-4be6-a821-64548ae9ecd3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"b8133605-fcee-4be6-a821-64548ae9ecd3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ac0149c1-6bd8-47fa-a197-3e9f5a337be9","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ac0149c1-6bd8-47fa-a197-3e9f5a337be9","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"ac0149c1-6bd8-47fa-a197-3e9f5a337be9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"616b10ee-a429-444d-a8bf-94cd8a855971","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"616b10ee-a429-444d-a8bf-94cd8a855971","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"616b10ee-a429-444d-a8bf-94cd8a855971","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"98ea7596-7327-4374-a15a-7eb692553a74","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"98ea7596-7327-4374-a15a-7eb692553a74","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"98ea7596-7327-4374-a15a-7eb692553a74","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"04f8755c-b3e3-4e05-a79d-27f37de59d14","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"04f8755c-b3e3-4e05-a79d-27f37de59d14","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"04f8755c-b3e3-4e05-a79d-27f37de59d14","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2be09aa3-b013-402b-94a5-bfe73c3c1e2d","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2be09aa3-b013-402b-94a5-bfe73c3c1e2d","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"2be09aa3-b013-402b-94a5-bfe73c3c1e2d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f0108cd2-bf49-4efb-a0fd-1629be3345c9","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f0108cd2-bf49-4efb-a0fd-1629be3345c9","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"f0108cd2-bf49-4efb-a0fd-1629be3345c9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e6c85afc-aa9c-45a8-a537-cbbbbf39c0b6","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e6c85afc-aa9c-45a8-a537-cbbbbf39c0b6","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","name":"e6c85afc-aa9c-45a8-a537-cbbbbf39c0b6","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 18.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 16.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"70638007-9100-4054-b519-eef76d46175a","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"bd058358-b81c-4ddf-a581-813faffb46ae","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fb39871f-f23f-4947-8641-d00598886359","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"42ad9958-ea32-4718-9ea9-8fb25d2764b5","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0422db68-3d18-45d1-b40f-5c4a7f145f3f","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6ece9dc2-d12e-4b8a-a633-1785a38ed0c7","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2f9ee163-55ed-401e-8734-7e9605147450","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77d8a375-7aca-4759-9e59-b6d57867f632","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"044c4172-d1bc-45d2-8633-747a027ed448","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dcd1c70e-e569-4f7a-ad51-a19750e3a157","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5e3371ad-2a9e-41c7-9af5-2149099ff91c","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"560de937-7562-45fe-9959-c819c0db2434","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"82abc0bf-5bcb-415f-9bee-dbe663e6aeb2","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5129112a-5ba9-4f0d-b693-6febc0004cf0","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"dff3d5a7-49a3-4e60-88f7-5cf97999ecc5","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ab28b515-b5ee-49d1-93b5-7161f29a7d15","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f884ab14-60e7-487e-98f6-4aa4fb3de01a","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b8133605-fcee-4be6-a821-64548ae9ecd3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1d66f221-d12d-4548-ab46-97344fb789d6","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ac0149c1-6bd8-47fa-a197-3e9f5a337be9","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9d3b4422-ee6c-4198-ba1b-20d7fb41f546","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"616b10ee-a429-444d-a8bf-94cd8a855971","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7f311cc3-f32e-45c0-ab78-d8b1c34807d5","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"98ea7596-7327-4374-a15a-7eb692553a74","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"14b56282-6fea-4050-89bd-e9fcf0d32189","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"04f8755c-b3e3-4e05-a79d-27f37de59d14","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8c99a59e-561b-4c81-bc8f-024e22a9759e","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2be09aa3-b013-402b-94a5-bfe73c3c1e2d","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"af87464e-46f9-4889-ac4f-d29fc7eb843e","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f0108cd2-bf49-4efb-a0fd-1629be3345c9","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b16a7136-da20-45f7-bde9-f799e3c508ad","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6c85afc-aa9c-45a8-a537-cbbbbf39c0b6","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 34,
    "yorigin": 63,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_z_stand_slash_mask_3","path":"sprites/spr_z_stand_slash_mask_3/spr_z_stand_slash_mask_3.yy",},
    "resourceVersion": "1.3",
    "name": "spr_z_stand_slash_mask_3",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Zero",
    "path": "folders/Sprites/Player/Zero.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_z_stand_slash_mask_3",
  "tags": [],
  "resourceType": "GMSprite",
}