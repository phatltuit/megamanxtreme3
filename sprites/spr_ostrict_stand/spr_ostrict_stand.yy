{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 46,
  "bbox_top": 0,
  "bbox_bottom": 64,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 47,
  "height": 65,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"6af15454-cfc1-4087-b684-886f6f32a4b2","path":"sprites/spr_ostrict_stand/spr_ostrict_stand.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6af15454-cfc1-4087-b684-886f6f32a4b2","path":"sprites/spr_ostrict_stand/spr_ostrict_stand.yy",},"LayerId":{"name":"e2a107cc-9179-4e17-a6ee-36e1acbe6231","path":"sprites/spr_ostrict_stand/spr_ostrict_stand.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ostrict_stand","path":"sprites/spr_ostrict_stand/spr_ostrict_stand.yy",},"resourceVersion":"1.0","name":"6af15454-cfc1-4087-b684-886f6f32a4b2","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ostrict_stand","path":"sprites/spr_ostrict_stand/spr_ostrict_stand.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"fe86a96f-3cba-4acd-b4dc-ac67e2c34a46","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6af15454-cfc1-4087-b684-886f6f32a4b2","path":"sprites/spr_ostrict_stand/spr_ostrict_stand.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 28,
    "yorigin": 57,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ostrict_stand","path":"sprites/spr_ostrict_stand/spr_ostrict_stand.yy",},
    "resourceVersion": "1.3",
    "name": "spr_ostrict_stand",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"e2a107cc-9179-4e17-a6ee-36e1acbe6231","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "QuickOstrict",
    "path": "folders/Sprites/Boss/QuickOstrict.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ostrict_stand",
  "tags": [],
  "resourceType": "GMSprite",
}