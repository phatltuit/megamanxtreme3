{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 30,
  "bbox_top": 0,
  "bbox_bottom": 43,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 31,
  "height": 44,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"70bde39a-d49c-4405-b027-e50caca709d5","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"70bde39a-d49c-4405-b027-e50caca709d5","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"LayerId":{"name":"bfb3c484-31fa-4c8c-9ba6-bfb0f9fc3c58","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_wall_jump","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"resourceVersion":"1.0","name":"70bde39a-d49c-4405-b027-e50caca709d5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"74c19025-9fcf-4ed6-88b9-f073bd68261a","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"74c19025-9fcf-4ed6-88b9-f073bd68261a","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"LayerId":{"name":"bfb3c484-31fa-4c8c-9ba6-bfb0f9fc3c58","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_wall_jump","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"resourceVersion":"1.0","name":"74c19025-9fcf-4ed6-88b9-f073bd68261a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ba_wall_jump","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 20.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 2.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"3ce219d6-abb6-46fb-a7a0-0ea641f60ff4","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"70bde39a-d49c-4405-b027-e50caca709d5","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ed58500c-3562-4cb0-a07e-8ea4a04f471d","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"74c19025-9fcf-4ed6-88b9-f073bd68261a","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 19,
    "yorigin": 31,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ba_wall_jump","path":"sprites/spr_ba_wall_jump/spr_ba_wall_jump.yy",},
    "resourceVersion": "1.3",
    "name": "spr_ba_wall_jump",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"bfb3c484-31fa-4c8c-9ba6-bfb0f9fc3c58","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Beta",
    "path": "folders/Sprites/Player/X/Armor/Beta.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ba_wall_jump",
  "tags": [],
  "resourceType": "GMSprite",
}