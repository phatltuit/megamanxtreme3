{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 32,
  "bbox_top": 0,
  "bbox_bottom": 47,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 33,
  "height": 48,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"f74f1ca4-6892-4a65-a85b-f99539d1a6d3","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f74f1ca4-6892-4a65-a85b-f99539d1a6d3","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"f74f1ca4-6892-4a65-a85b-f99539d1a6d3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c522fcbe-8356-4386-95ee-cb3a8ade7454","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c522fcbe-8356-4386-95ee-cb3a8ade7454","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"c522fcbe-8356-4386-95ee-cb3a8ade7454","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0e267d09-e56b-4341-9e7f-7f11041635b5","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0e267d09-e56b-4341-9e7f-7f11041635b5","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"0e267d09-e56b-4341-9e7f-7f11041635b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b0dc314d-c97e-4f60-8d43-b449d9d5dae2","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b0dc314d-c97e-4f60-8d43-b449d9d5dae2","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"b0dc314d-c97e-4f60-8d43-b449d9d5dae2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b8c25540-aa80-4262-9f33-b4a503d6a564","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b8c25540-aa80-4262-9f33-b4a503d6a564","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"b8c25540-aa80-4262-9f33-b4a503d6a564","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f89de347-b92f-4625-94c2-175c0ffb3eb5","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f89de347-b92f-4625-94c2-175c0ffb3eb5","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"f89de347-b92f-4625-94c2-175c0ffb3eb5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"25512781-d5be-4fe8-a7bb-e2a4a2f50077","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"25512781-d5be-4fe8-a7bb-e2a4a2f50077","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"25512781-d5be-4fe8-a7bb-e2a4a2f50077","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c54bcac1-22f2-457b-8baa-3d151ab56759","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c54bcac1-22f2-457b-8baa-3d151ab56759","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"c54bcac1-22f2-457b-8baa-3d151ab56759","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"8eca7f05-9d06-4f98-8bcd-b25dcd18b8d3","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8eca7f05-9d06-4f98-8bcd-b25dcd18b8d3","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"8eca7f05-9d06-4f98-8bcd-b25dcd18b8d3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a1535793-b0ae-46fc-a277-aa0347116815","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a1535793-b0ae-46fc-a277-aa0347116815","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"a1535793-b0ae-46fc-a277-aa0347116815","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e21561ac-3eae-431f-8ba8-70b0b904902b","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e21561ac-3eae-431f-8ba8-70b0b904902b","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"LayerId":{"name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","name":"e21561ac-3eae-431f-8ba8-70b0b904902b","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 20.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 11.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"a473c8cd-6c81-4d98-9e33-58c8e37a3d88","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f74f1ca4-6892-4a65-a85b-f99539d1a6d3","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4cd19801-56d0-4924-98ac-3e00a198984a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c522fcbe-8356-4386-95ee-cb3a8ade7454","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"ae4d212d-c760-46da-bd14-165a33f8001b","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0e267d09-e56b-4341-9e7f-7f11041635b5","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a4ac8e55-4b79-4fec-a602-c0f92becaf21","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b0dc314d-c97e-4f60-8d43-b449d9d5dae2","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"3e670b77-d377-485a-aa25-ec7987f9ac15","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b8c25540-aa80-4262-9f33-b4a503d6a564","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e484f603-02e3-4c85-a8c2-f4d25d5947ed","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f89de347-b92f-4625-94c2-175c0ffb3eb5","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"73d7b902-3d87-4932-b6f9-d040dfe2ea9b","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"25512781-d5be-4fe8-a7bb-e2a4a2f50077","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5aa01c23-dfdf-43b0-b49e-3ca14801b58c","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c54bcac1-22f2-457b-8baa-3d151ab56759","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"da3b5282-2f15-4624-9296-9e3d6d022e73","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8eca7f05-9d06-4f98-8bcd-b25dcd18b8d3","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"88c0470b-36ae-4baf-a849-e7176243f4cd","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a1535793-b0ae-46fc-a277-aa0347116815","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"710c97e4-b8a0-4284-822c-ab609b6bd82d","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e21561ac-3eae-431f-8ba8-70b0b904902b","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 15,
    "yorigin": 40,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ba_victory","path":"sprites/spr_ba_victory/spr_ba_victory.yy",},
    "resourceVersion": "1.3",
    "name": "spr_ba_victory",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"daf0d760-a075-46b5-82f9-7624cf9f2cb7","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Beta",
    "path": "folders/Sprites/Player/X/Armor/Beta.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ba_victory",
  "tags": [],
  "resourceType": "GMSprite",
}