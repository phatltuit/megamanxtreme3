{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 33,
  "bbox_top": 1,
  "bbox_bottom": 36,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 34,
  "height": 37,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": [
    4278190335,
    4278255615,
    4278255360,
    4294967040,
    4294901760,
    4294902015,
    4286591008,
    4294475776,
    4293059298,
    4292335575,
    4291677645,
    4290230199,
    4287993237,
    4280556782,
    4278252287,
    4283540992,
    4293963264,
    4287770926,
    4287365357,
    4287203721,
    4286414205,
    4285558896,
    4284703587,
    4283782485,
    4281742902,
    4278190080,
    4286158839,
    4286688762,
    4287219453,
    4288280831,
    4288405444,
    4288468131,
    4288465538,
    4291349882,
    4294430829,
    4292454269,
    4291466115,
    4290675079,
    4290743485,
    4290943732,
    4288518390,
    4283395315,
    4283862775,
    4284329979,
    4285068799,
    4285781164,
    4285973884,
    4286101564,
    4290034460,
    4294164224,
    4291529796,
    4289289312,
    4289290373,
    4289291432,
    4289359601,
    4286410226,
    4280556782,
    4280444402,
    4280128760,
    4278252287,
    4282369933,
    4283086137,
    4283540992,
    4288522496,
    4293963264,
    4290540032,
    4289423360,
    4289090560,
    4287770926,
    4287704422,
    4287571858,
    4287365357,
    4284159214,
    4279176094,
    4279058848,
    4278870691,
    4278231211,
    4281367321,
  ],
  "gridX": 4,
  "gridY": 16,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"4f15dac2-35f3-4a05-832b-81da858f7bbe","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4f15dac2-35f3-4a05-832b-81da858f7bbe","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"4f15dac2-35f3-4a05-832b-81da858f7bbe","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e9252e0e-5276-4c22-8d57-ac35fec15723","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e9252e0e-5276-4c22-8d57-ac35fec15723","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"e9252e0e-5276-4c22-8d57-ac35fec15723","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"99bfdf35-4c15-491b-bcee-f5e7dfc000df","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"99bfdf35-4c15-491b-bcee-f5e7dfc000df","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"99bfdf35-4c15-491b-bcee-f5e7dfc000df","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"03d579c0-22ea-4476-9d32-69fe322c760f","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"03d579c0-22ea-4476-9d32-69fe322c760f","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"03d579c0-22ea-4476-9d32-69fe322c760f","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"29ad9242-e23b-4f50-9ab4-47eb8629f934","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"29ad9242-e23b-4f50-9ab4-47eb8629f934","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"29ad9242-e23b-4f50-9ab4-47eb8629f934","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"c6035a24-1cd3-4b93-ac1a-9bc9046151ca","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"c6035a24-1cd3-4b93-ac1a-9bc9046151ca","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"c6035a24-1cd3-4b93-ac1a-9bc9046151ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ca255c81-22b1-4df9-bae5-5c05b1b14a0b","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ca255c81-22b1-4df9-bae5-5c05b1b14a0b","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"ca255c81-22b1-4df9-bae5-5c05b1b14a0b","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a9ca5fc0-5d03-40db-830e-5c3d971d0dcf","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a9ca5fc0-5d03-40db-830e-5c3d971d0dcf","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"a9ca5fc0-5d03-40db-830e-5c3d971d0dcf","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f3a8e42f-7e14-46eb-8c8a-f35c4ae825ca","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f3a8e42f-7e14-46eb-8c8a-f35c4ae825ca","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"f3a8e42f-7e14-46eb-8c8a-f35c4ae825ca","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"91c787bf-86ad-4363-bc8a-e7a60b66a5b7","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"91c787bf-86ad-4363-bc8a-e7a60b66a5b7","path":"sprites/spr_ba_run/spr_ba_run.yy",},"LayerId":{"name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","name":"91c787bf-86ad-4363-bc8a-e7a60b66a5b7","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 20.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 10.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5acce469-9436-4d52-8dbf-28ee7fcf693b","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4f15dac2-35f3-4a05-832b-81da858f7bbe","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e1890f79-3d70-43a1-a1c9-d23f02e08b4f","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e9252e0e-5276-4c22-8d57-ac35fec15723","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b653d6db-5d50-4303-a3be-78dbb435bd34","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"99bfdf35-4c15-491b-bcee-f5e7dfc000df","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2141ff1a-4d10-4957-b191-149937c9359c","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"03d579c0-22ea-4476-9d32-69fe322c760f","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a8018c10-84eb-43b0-a30e-c3b4eab7b69f","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"29ad9242-e23b-4f50-9ab4-47eb8629f934","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"54ff92c8-a2eb-4ba7-8e45-1cfb37f1669a","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"c6035a24-1cd3-4b93-ac1a-9bc9046151ca","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"09a8877e-270c-4690-8a9e-245857462d89","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ca255c81-22b1-4df9-bae5-5c05b1b14a0b","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"15eb3c21-312f-4837-94bc-fad15dbe50de","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a9ca5fc0-5d03-40db-830e-5c3d971d0dcf","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f6d12235-28e1-4c8d-a0df-74d749689e25","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f3a8e42f-7e14-46eb-8c8a-f35c4ae825ca","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8e86ec2d-16c8-4c2e-af09-ea9b41e025b7","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"91c787bf-86ad-4363-bc8a-e7a60b66a5b7","path":"sprites/spr_ba_run/spr_ba_run.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 21,
    "yorigin": 29,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_ba_run","path":"sprites/spr_ba_run/spr_ba_run.yy",},
    "resourceVersion": "1.3",
    "name": "spr_ba_run",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"ca1b71f7-082a-4817-b1b2-a35be9d0bb87","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Beta",
    "path": "folders/Sprites/Player/X/Armor/Beta.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_ba_run",
  "tags": [],
  "resourceType": "GMSprite",
}