{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 55,
  "bbox_top": 0,
  "bbox_bottom": 46,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 58,
  "height": 48,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"d3890545-ff7b-434c-8001-8614824869ed","path":"sprites/spr_panther_got_hit/spr_panther_got_hit.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d3890545-ff7b-434c-8001-8614824869ed","path":"sprites/spr_panther_got_hit/spr_panther_got_hit.yy",},"LayerId":{"name":"59576e7d-e78c-442d-bcbf-150db1d2215b","path":"sprites/spr_panther_got_hit/spr_panther_got_hit.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_panther_got_hit","path":"sprites/spr_panther_got_hit/spr_panther_got_hit.yy",},"resourceVersion":"1.0","name":"d3890545-ff7b-434c-8001-8614824869ed","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_panther_got_hit","path":"sprites/spr_panther_got_hit/spr_panther_got_hit.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 30.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 1.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"37bc4dad-e936-4aad-af03-b0e622153738","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d3890545-ff7b-434c-8001-8614824869ed","path":"sprites/spr_panther_got_hit/spr_panther_got_hit.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 34,
    "yorigin": 35,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_panther_got_hit","path":"sprites/spr_panther_got_hit/spr_panther_got_hit.yy",},
    "resourceVersion": "1.3",
    "name": "spr_panther_got_hit",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"59576e7d-e78c-442d-bcbf-150db1d2215b","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "GoldenFlauclawPanther",
    "path": "folders/Sprites/Boss/GoldenFlauclawPanther.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_panther_got_hit",
  "tags": [],
  "resourceType": "GMSprite",
}