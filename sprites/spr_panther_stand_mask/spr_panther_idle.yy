{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 2,
  "bbox_right": 59,
  "bbox_top": 18,
  "bbox_bottom": 59,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 62,
  "height": 60,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"8978b4dd-2f8c-423f-a50c-08ececa6b020","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"8978b4dd-2f8c-423f-a50c-08ececa6b020","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"LayerId":{"name":"4b0b4f0c-2408-477a-96b1-d4c9d3c9a08f","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_panther_idle","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","name":"8978b4dd-2f8c-423f-a50c-08ececa6b020","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"7a949082-26a5-4a37-be54-33d3821bee02","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"7a949082-26a5-4a37-be54-33d3821bee02","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"LayerId":{"name":"4b0b4f0c-2408-477a-96b1-d4c9d3c9a08f","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_panther_idle","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","name":"7a949082-26a5-4a37-be54-33d3821bee02","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"1e8a7813-4f56-45a1-9b40-9d7dcaad1817","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"1e8a7813-4f56-45a1-9b40-9d7dcaad1817","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"LayerId":{"name":"4b0b4f0c-2408-477a-96b1-d4c9d3c9a08f","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_panther_idle","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","name":"1e8a7813-4f56-45a1-9b40-9d7dcaad1817","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"501ba74c-809c-4ada-b062-3c43951633df","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"501ba74c-809c-4ada-b062-3c43951633df","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"LayerId":{"name":"4b0b4f0c-2408-477a-96b1-d4c9d3c9a08f","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_panther_idle","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","name":"501ba74c-809c-4ada-b062-3c43951633df","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_panther_idle","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 4.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"aaca0b4b-5ad3-4228-8dc8-a9ec12f41c06","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"8978b4dd-2f8c-423f-a50c-08ececa6b020","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"1ec8bf9b-bee0-467d-8e10-b5cce841f805","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"7a949082-26a5-4a37-be54-33d3821bee02","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"de14d33b-7d3b-430a-a24a-e537943f6cb1","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"1e8a7813-4f56-45a1-9b40-9d7dcaad1817","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"0731952a-3527-4bdf-a1b9-bf4beda92d14","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"501ba74c-809c-4ada-b062-3c43951633df","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 27,
    "yorigin": 51,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_panther_idle","path":"sprites/spr_panther_idle/spr_panther_idle.yy",},
    "resourceVersion": "1.3",
    "name": "spr_panther_idle",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"4b0b4f0c-2408-477a-96b1-d4c9d3c9a08f","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "ThunderPanther",
    "path": "folders/Sprites/Boss/ThunderPanther.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_panther_idle",
  "tags": [],
  "resourceType": "GMSprite",
}