{
  "bboxMode": 0,
  "collisionKind": 4,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 1,
  "bbox_right": 93,
  "bbox_top": 7,
  "bbox_bottom": 98,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 94,
  "height": 99,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": [
    4278190335,
    4278255615,
    4278255360,
    4294967040,
    4294901760,
    4294902015,
    4282425408,
    4293717228,
    4293059298,
    4292335575,
    4291677645,
    4290230199,
    4287993237,
    4280556782,
    4278252287,
    4283540992,
    4293963264,
    4287770926,
    4287365357,
    4287203721,
    4286414205,
    4285558896,
    4284703587,
    4283782485,
    4281742902,
    4278190080,
    4286158839,
    4286688762,
    4287219453,
    4288280831,
    4288405444,
    4288468131,
    4288465538,
    4291349882,
    4294430829,
    4292454269,
    4291466115,
    4290675079,
    4290743485,
    4290943732,
    4288518390,
    4283395315,
    4283862775,
    4284329979,
    4285068799,
    4285781164,
    4285973884,
    4286101564,
    4290034460,
    4294164224,
    4291529796,
    4289289312,
    4289290373,
    4289291432,
    4289359601,
    4286410226,
    4280556782,
    4280444402,
    4280128760,
    4278252287,
    4282369933,
    4283086137,
    4283540992,
    4288522496,
    4293963264,
    4290540032,
    4289423360,
    4289090560,
    4287770926,
    4287704422,
    4287571858,
    4287365357,
    4284159214,
    4279176094,
    4279058848,
    4278870691,
    4278231211,
    4281367321,
  ],
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"743fc952-aede-448c-ad7c-0d5eba593008","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"743fc952-aede-448c-ad7c-0d5eba593008","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"743fc952-aede-448c-ad7c-0d5eba593008","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"57b5bbb6-dee0-41e6-bfac-916e461cb142","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"57b5bbb6-dee0-41e6-bfac-916e461cb142","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"57b5bbb6-dee0-41e6-bfac-916e461cb142","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"acc87be8-f5cf-4c00-b478-a483837f4474","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"acc87be8-f5cf-4c00-b478-a483837f4474","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"acc87be8-f5cf-4c00-b478-a483837f4474","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"555218cc-e8ff-4cef-94c0-114c3d496505","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"555218cc-e8ff-4cef-94c0-114c3d496505","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"555218cc-e8ff-4cef-94c0-114c3d496505","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e2e58078-b3da-42bb-9702-9d666ddf40a2","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e2e58078-b3da-42bb-9702-9d666ddf40a2","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"e2e58078-b3da-42bb-9702-9d666ddf40a2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9a2d7065-ab6b-4ec1-a486-847e784b9533","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9a2d7065-ab6b-4ec1-a486-847e784b9533","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"9a2d7065-ab6b-4ec1-a486-847e784b9533","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2d5a3c3c-b669-4eed-ab8f-72a3d48478e7","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2d5a3c3c-b669-4eed-ab8f-72a3d48478e7","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"2d5a3c3c-b669-4eed-ab8f-72a3d48478e7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fd34b27a-aefc-4c5a-acb0-9eb10c4f1bba","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fd34b27a-aefc-4c5a-acb0-9eb10c4f1bba","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"fd34b27a-aefc-4c5a-acb0-9eb10c4f1bba","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d23bb165-573e-4661-a0aa-1af34a92a1f8","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d23bb165-573e-4661-a0aa-1af34a92a1f8","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"d23bb165-573e-4661-a0aa-1af34a92a1f8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a086e241-b286-49c9-a0d6-6430da779cbc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a086e241-b286-49c9-a0d6-6430da779cbc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"a086e241-b286-49c9-a0d6-6430da779cbc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"52550edc-ef3e-4c6c-99e6-dab32f9f9216","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"52550edc-ef3e-4c6c-99e6-dab32f9f9216","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"52550edc-ef3e-4c6c-99e6-dab32f9f9216","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d8c8b2e5-fff1-4aac-86b4-7daa43524424","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d8c8b2e5-fff1-4aac-86b4-7daa43524424","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"d8c8b2e5-fff1-4aac-86b4-7daa43524424","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5dd9257a-6484-47f8-b04c-b0df8e71119a","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5dd9257a-6484-47f8-b04c-b0df8e71119a","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"5dd9257a-6484-47f8-b04c-b0df8e71119a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fe4ab9b5-b50d-40c0-875c-7890493cd6f7","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fe4ab9b5-b50d-40c0-875c-7890493cd6f7","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"fe4ab9b5-b50d-40c0-875c-7890493cd6f7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"058ad56d-21f1-4840-8131-c33d72a9f0d4","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"058ad56d-21f1-4840-8131-c33d72a9f0d4","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"058ad56d-21f1-4840-8131-c33d72a9f0d4","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"fc7348d5-d3f1-4447-9164-a22255e7220a","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"fc7348d5-d3f1-4447-9164-a22255e7220a","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"LayerId":{"name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","name":"fc7348d5-d3f1-4447-9164-a22255e7220a","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 18.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 16.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"163f8355-1035-43fd-8ee7-218dac59176e","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"743fc952-aede-448c-ad7c-0d5eba593008","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"78b6170e-ba7c-4b9b-8ff6-1db43ccf221a","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"57b5bbb6-dee0-41e6-bfac-916e461cb142","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fe0c0811-03fd-4207-90c1-d1895244ece9","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"acc87be8-f5cf-4c00-b478-a483837f4474","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"64dcbde8-380d-43ec-aeb3-417673e812a3","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"555218cc-e8ff-4cef-94c0-114c3d496505","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"439df502-cca0-425b-9515-85d773470aff","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e2e58078-b3da-42bb-9702-9d666ddf40a2","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b8639f0c-2b1a-49ee-b955-d29b54e46276","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9a2d7065-ab6b-4ec1-a486-847e784b9533","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"61ade760-1d4d-4277-a26b-42764dbf296e","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2d5a3c3c-b669-4eed-ab8f-72a3d48478e7","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"79134949-d5ed-4eba-94b6-0d7cdbf45db4","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fd34b27a-aefc-4c5a-acb0-9eb10c4f1bba","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7130b288-0dfa-44e7-90a1-1b2290ec414a","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d23bb165-573e-4661-a0aa-1af34a92a1f8","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c09a085f-b11b-4d8b-a661-9476b79ff20f","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a086e241-b286-49c9-a0d6-6430da779cbc","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"660b648b-c868-4f08-8745-d90d137dd8f3","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"52550edc-ef3e-4c6c-99e6-dab32f9f9216","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf50e3dc-4a0e-4ddc-9da4-18efa1c477de","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d8c8b2e5-fff1-4aac-86b4-7daa43524424","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f8d7e05b-6ff4-4ade-bd66-727ee456936d","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5dd9257a-6484-47f8-b04c-b0df8e71119a","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"93b0f633-0808-4cba-a1b9-fce3d2668609","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fe4ab9b5-b50d-40c0-875c-7890493cd6f7","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4ef97216-79c4-4f48-a0d8-0cbb9042f46a","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"058ad56d-21f1-4840-8131-c33d72a9f0d4","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a1a7e46-6d85-4da3-b735-9986eec3ee5d","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"fc7348d5-d3f1-4447-9164-a22255e7220a","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 48,
    "yorigin": 66,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_z_roll_slash_mask","path":"sprites/spr_z_roll_slash_mask/spr_z_roll_slash_mask.yy",},
    "resourceVersion": "1.3",
    "name": "spr_z_roll_slash_mask",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"fe003dfb-8140-4515-a987-60b9eca1e7bc","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Zero",
    "path": "folders/Sprites/Player/Zero.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_z_roll_slash_mask",
  "tags": [],
  "resourceType": "GMSprite",
}