{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 9,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 0,
  "bbox_right": 117,
  "bbox_top": 0,
  "bbox_bottom": 86,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 118,
  "height": 90,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": [
    4278190335,
    4278255615,
    4278255360,
    4294967040,
    4294901760,
    4294902015,
    4278196444,
    4288733344,
    4288733344,
    4278198496,
    4291677645,
    4290230199,
    4287993237,
    4280556782,
    4278252287,
    4283540992,
    4293963264,
    4287770926,
    4287365357,
    4287203721,
    4286414205,
    4285558896,
    4284703587,
    4283782485,
    4281742902,
    4278190080,
    4286158839,
    4286688762,
    4287219453,
    4288280831,
    4288405444,
    4288468131,
    4288465538,
    4291349882,
    4294430829,
    4292454269,
    4291466115,
    4290675079,
    4290743485,
    4290943732,
    4288518390,
    4283395315,
    4283862775,
    4284329979,
    4285068799,
    4285781164,
    4285973884,
    4286101564,
    4290034460,
    4294164224,
    4291529796,
    4289289312,
    4289290373,
    4289291432,
    4289359601,
    4286410226,
    4280556782,
    4280444402,
    4280128760,
    4278252287,
    4282369933,
    4283086137,
    4283540992,
    4288522496,
    4293963264,
    4290540032,
    4289423360,
    4289090560,
    4287770926,
    4287704422,
    4287571858,
    4287365357,
    4284159214,
    4279176094,
    4279058848,
    4278870691,
    4278231211,
    4281367321,
  ],
  "gridX": 4,
  "gridY": 4,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e6c85afc-aa9c-45a8-a537-cbbbbf39c0b6","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e6c85afc-aa9c-45a8-a537-cbbbbf39c0b6","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"e6c85afc-aa9c-45a8-a537-cbbbbf39c0b6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"42ad9958-ea32-4718-9ea9-8fb25d2764b5","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"42ad9958-ea32-4718-9ea9-8fb25d2764b5","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"42ad9958-ea32-4718-9ea9-8fb25d2764b5","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"6ece9dc2-d12e-4b8a-a633-1785a38ed0c7","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"6ece9dc2-d12e-4b8a-a633-1785a38ed0c7","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"6ece9dc2-d12e-4b8a-a633-1785a38ed0c7","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"77d8a375-7aca-4759-9e59-b6d57867f632","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"77d8a375-7aca-4759-9e59-b6d57867f632","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"77d8a375-7aca-4759-9e59-b6d57867f632","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"dcd1c70e-e569-4f7a-ad51-a19750e3a157","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"dcd1c70e-e569-4f7a-ad51-a19750e3a157","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"dcd1c70e-e569-4f7a-ad51-a19750e3a157","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"560de937-7562-45fe-9959-c819c0db2434","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"560de937-7562-45fe-9959-c819c0db2434","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"560de937-7562-45fe-9959-c819c0db2434","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"5129112a-5ba9-4f0d-b693-6febc0004cf0","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"5129112a-5ba9-4f0d-b693-6febc0004cf0","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"5129112a-5ba9-4f0d-b693-6febc0004cf0","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ab28b515-b5ee-49d1-93b5-7161f29a7d15","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ab28b515-b5ee-49d1-93b5-7161f29a7d15","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"ab28b515-b5ee-49d1-93b5-7161f29a7d15","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"b8133605-fcee-4be6-a821-64548ae9ecd3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"b8133605-fcee-4be6-a821-64548ae9ecd3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"b8133605-fcee-4be6-a821-64548ae9ecd3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ac0149c1-6bd8-47fa-a197-3e9f5a337be9","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ac0149c1-6bd8-47fa-a197-3e9f5a337be9","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"ac0149c1-6bd8-47fa-a197-3e9f5a337be9","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"616b10ee-a429-444d-a8bf-94cd8a855971","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"616b10ee-a429-444d-a8bf-94cd8a855971","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"616b10ee-a429-444d-a8bf-94cd8a855971","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"98ea7596-7327-4374-a15a-7eb692553a74","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"98ea7596-7327-4374-a15a-7eb692553a74","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"98ea7596-7327-4374-a15a-7eb692553a74","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"04f8755c-b3e3-4e05-a79d-27f37de59d14","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"04f8755c-b3e3-4e05-a79d-27f37de59d14","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"04f8755c-b3e3-4e05-a79d-27f37de59d14","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"2be09aa3-b013-402b-94a5-bfe73c3c1e2d","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"2be09aa3-b013-402b-94a5-bfe73c3c1e2d","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"2be09aa3-b013-402b-94a5-bfe73c3c1e2d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f0108cd2-bf49-4efb-a0fd-1629be3345c9","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f0108cd2-bf49-4efb-a0fd-1629be3345c9","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"LayerId":{"name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","name":"f0108cd2-bf49-4efb-a0fd-1629be3345c9","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 18.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 15.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"73689bb7-e4db-4afb-adab-f720419885a7","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e6c85afc-aa9c-45a8-a537-cbbbbf39c0b6","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"fd323548-1bf5-4cc8-b4c5-32e984292c59","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"42ad9958-ea32-4718-9ea9-8fb25d2764b5","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"2770ea33-03b4-49c7-b3e3-c2783a2262fc","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"6ece9dc2-d12e-4b8a-a633-1785a38ed0c7","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9e67f4de-7369-4d67-b037-6b71ce4a21d0","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"77d8a375-7aca-4759-9e59-b6d57867f632","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"80b5a65e-94ba-4a49-b730-bd641cfbbce2","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"dcd1c70e-e569-4f7a-ad51-a19750e3a157","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"96257cdc-51f9-45ec-952c-2b0509ec0bf9","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"560de937-7562-45fe-9959-c819c0db2434","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"9e92527d-801e-4982-b423-fa92ba408ed8","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"5129112a-5ba9-4f0d-b693-6febc0004cf0","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a40c999d-307d-43f2-af73-5fa9e17abd4e","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ab28b515-b5ee-49d1-93b5-7161f29a7d15","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5cb8cd22-8e17-44eb-aaef-afab75c5443e","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"b8133605-fcee-4be6-a821-64548ae9ecd3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7a8c76eb-705d-4a21-9fad-641d93865bc1","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ac0149c1-6bd8-47fa-a197-3e9f5a337be9","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"023ec521-75cb-497a-a15c-0131e4c03efe","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"616b10ee-a429-444d-a8bf-94cd8a855971","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8bb37440-8a2e-499e-9d95-0b6988f0700e","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"98ea7596-7327-4374-a15a-7eb692553a74","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"11088739-a606-4bfc-a3b9-a3101ea041b5","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"04f8755c-b3e3-4e05-a79d-27f37de59d14","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a7208134-b915-4f66-b519-26379783631c","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"2be09aa3-b013-402b-94a5-bfe73c3c1e2d","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d3f34819-7994-4443-84b0-e1ab0dba3f6a","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f0108cd2-bf49-4efb-a0fd-1629be3345c9","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": true,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 34,
    "yorigin": 63,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_z_stand_slash_3","path":"sprites/spr_z_stand_slash_3/spr_z_stand_slash_3.yy",},
    "resourceVersion": "1.3",
    "name": "spr_z_stand_slash_3",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"6e5ddae4-e2d5-4aa9-a05d-0b18cda9370c","tags":[],"resourceType":"GMImageLayer",},
  ],
  "parent": {
    "name": "Zero",
    "path": "folders/Sprites/Player/Zero.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_z_stand_slash_3",
  "tags": [],
  "resourceType": "GMSprite",
}